<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_booking_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('biName',200)->nullable();
            $table->text('biDescription')->nullable();
            $table->tinyinteger('biBranchId')->nullable();
            $table->string('createdBy',6)->nullable();
            $table->string('status',1)->default(1)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_booking_items');
    }
}
