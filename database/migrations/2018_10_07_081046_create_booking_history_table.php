<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_booking_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('biId', 6)->nullable();
            $table->string('bookedBy',200)->nullable();
            $table->string('purpose',200)->nullable();
            $table->date('biDate')->nullable();
            $table->string('biStartTime')->nullable();
            $table->string('biEndTime')->nullable();
            $table->string('status',1)->nullable();
            $table->string('createdBy', 6)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_booking_history');
    }
}
