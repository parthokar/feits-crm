<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_vehicle_booking', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('vehicle_id',6)->nullable();
            $table->string('booked_by',200)->nullable();
            $table->string('driver_name',200)->nullable();
            $table->text('purpose')->nullable();
            $table->date('vb_date')->nullable();
            $table->string('vb_startTime')->nullable();
            $table->string('vb_EndTime')->nullable();
            $table->string('status',1)->nullable();
            $table->string('created_by',6)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_vehicle_booking');
    }
}
