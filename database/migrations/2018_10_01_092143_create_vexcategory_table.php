<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVexcategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_vexcategory', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('vcategory_name',150)->nullable();
            $table->text('vcat_description')->nullable();
            $table->string('created_by',6)->nullable();
            $table->string('status',1)->default(1)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_vexcategory');
    }
}
