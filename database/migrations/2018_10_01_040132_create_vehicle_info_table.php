<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_vehicle_info', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('vehicle_name',150)->nullable();
            $table->string('engine_machine_number',120)->nullable();
            $table->string('registration_number',120)->nullable();
            $table->string('insurance_number',120)->nullable();
            $table->text('remarks')->nullable();
            $table->string('created_by', 6)->nullable();
            $table->tinyinteger('branch_id')->nullable();
            $table->string('status',1)->default(1)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_vehicle_info');
    }
}
