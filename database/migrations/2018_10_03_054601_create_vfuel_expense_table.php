<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVfuelExpenseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_vfuel_expense', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyinteger('vehicle_id')->nullable();
            $table->tinyinteger('fuel_type')->nullable();
            $table->string('vex_references')->nullable();
            $table->string('quantity',10)->nullable();
            $table->string('amount',10)->nullable();
            $table->date('vex_date')->nullable();
            $table->text('vex_description')->nullable();
            $table->text('vex_attachment')->nullable();
            $table->tinyinteger('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_vfuel_expense');
    }
}
