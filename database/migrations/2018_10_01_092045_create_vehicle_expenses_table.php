<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_vehicle_expenses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('vehicle_id', 6)->nullable();
            $table->string('vex_category_id', 6)->nullable();
            $table->string('vex_references')->nullable();
            $table->string('amount',10)->nullable();
            $table->date('vex_date')->nullable();
            $table->text('vex_description')->nullable();
            $table->text('vex_attachment')->nullable();
            $table->string('created_by', 6)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_vehicle_expenses');
    }
}
