<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Carbon\Carbon;
use Validator;
use Session;
use Illuminate\Support\Facades\Redirect;
use App\Repositories\Settings;

class FileShareController extends Controller
{
    protected $fileShare;
    public function __construct()
    {
        // create object of settings class
        $this->fileShare = new Settings();
    }

    /**
     * file share view.
     */
    public function index(){

      $haserole = $this->checksuperadmin(['employee']);
       if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('super-admin')){
            $employee=$this->fileShare->all_employee();
            $branch=$this->fileShare->all_branch();
            $department=$this->fileShare->all_department();
            $designation=$this->fileShare->all_designation();
       }
        if($haserole){
            $branch=$this->fileShare->employeeBranch();
            $employee=$this->fileShare->all_employee();
            $department=$this->fileShare->employeeDepartment();
            $designation=$this->fileShare->employeeDesignation();
        }if(auth()->user()->hasRole('branch-manager')){
            $employee=$this->fileShare->branchall_employee();
            $branch=$this->fileShare->branchname_loginemployee();
            $department=$this->fileShare->employeeDepartmentBranch();
            $designation=$this->fileShare->all_designation();
        }
        return view('backend.file_share.index',compact('employee','branch','department','designation','haserole'));
    }


    /**
     * file store to database.
     */
    public function fileStore(Request $request){

        if($request->type_id==1){
            $rules = array(
                'emp_id'=>'required',
                'attachment' => 'required',
            );
            $error = Validator::make($request->all(), $rules);
            if($error->fails())
            {
                return response()->json(['errors' => $error->errors()->all()]);
            }
          if($request->hasfile('attachment'))
          {
                foreach($request->file('attachment') as $image)
                {
                    $name=time().'.'.$image->getClientOriginalName();
                    $destinationPath = 'share_file';
                    $image->move($destinationPath, $name);
                    $data[] = $name;
                    $insert=DB::table('tb_file_sharing')->insert([
                    'shared_group_type' =>$request->type_id,
                    'referenceId' =>$request->emp_id,
                    'shared_date' =>date('Y-m-d'),
                    'attachment' =>$name,
                    'emp_id' =>$request->emp_id,
                    'shared_by' =>auth()->user()->id,
                    'created_at'=>Carbon::now()->toDateTimeString(),
                    'updated_at'=>Carbon::now()->toDateTimeString()
                ]);

                }
               return response()->json(['success' => 'File has been share successfully']);
           }
        }


        if($request->type_id==2){
            $rules = array(
                'emp_id'=>'required',
                'attachment'=>'required',
            );
            $error = Validator::make($request->all(), $rules);
            if($error->fails())
            {
                return response()->json(['errors' => $error->errors()->all()]);
            }
            if($request->hasfile('attachment')) {
                foreach ($request->file('attachment') as $image) {
                    $name=time().'.'.$image->getClientOriginalName();
                    $destinationPath = 'share_file';
                    $image->move($destinationPath, $name);
                    $data[] = $name;
                    foreach ($request->emp_id as $employee) {
                        $insert = DB::table('tb_file_sharing')->insert([
                            'shared_group_type' => $request->type_id,
                            'referenceId' => $employee,
                            'shared_date' => date('Y-m-d'),
                            'attachment' => $name,
                            'emp_id' => $employee,
                            'shared_by' =>auth()->user()->id,
                            'created_at' => Carbon::now()->toDateTimeString(),
                            'updated_at' => Carbon::now()->toDateTimeString()
                        ]);
                    }
                }
                return response()->json(['success' => 'File has been share successfully']);
            }
        }


        if($request->type_id==3){
            $rules = array(
                'branch_id'=>'required',
                'attachment'=>'required',
            );
            $error = Validator::make($request->all(), $rules);
            if($error->fails())
            {
                return response()->json(['errors' => $error->errors()->all()]);
            }
            if($request->hasfile('attachment'))
            {
                foreach($request->file('attachment') as $image)
                {
                    $name=time().'.'.$image->getClientOriginalName();
                    $destinationPath = 'share_file';
                    $image->move($destinationPath, $name);
                    $data[] = $name;
                    $insert=DB::table('tb_file_sharing')->insert([
                        'shared_group_type' =>$request->type_id,
                        'referenceId' =>$request->branch_id,
                        'shared_date' =>date('Y-m-d'),
                        'attachment' =>$name,
                        'branch_id' =>$request->branch_id,
                        'shared_by' =>auth()->user()->id,
                        'created_at'=>Carbon::now()->toDateTimeString(),
                        'updated_at'=>Carbon::now()->toDateTimeString()
                    ]);

                }
                return response()->json(['success' => 'File has been share successfully']);
            }
        }

        if($request->type_id==4){
            $rules = array(
                'dept_id'=>'required',
                'attachment'=>'required',
            );
            $error = Validator::make($request->all(), $rules);
            if($error->fails())
            {
                return response()->json(['errors' => $error->errors()->all()]);
            }
            if($request->hasfile('attachment'))
            {
                foreach($request->file('attachment') as $image)
                {
                    $name=time().'.'.$image->getClientOriginalName();
                    $destinationPath = 'share_file';
                    $image->move($destinationPath, $name);
                    $data[] = $name;
                    $insert=DB::table('tb_file_sharing')->insert([
                        'shared_group_type' =>$request->type_id,
                        'referenceId' =>$request->dept_id,
                        'shared_date' =>date('Y-m-d'),
                        'attachment' =>$name,
                        'department_id' =>$request->dept_id,
                        'shared_by' =>auth()->user()->id,
                        'created_at'=>Carbon::now()->toDateTimeString(),
                        'updated_at'=>Carbon::now()->toDateTimeString()
                    ]);

                }
                return response()->json(['success' => 'File has been share successfully']);
            }
        }


        if($request->type_id==5){
            $rules = array(
                'designation_id'=>'required',
                'attachment'=>'required',
            );
            $error = Validator::make($request->all(), $rules);
            if($error->fails())
            {
                return response()->json(['errors' => $error->errors()->all()]);
            }
            if($request->hasfile('attachment'))
            {
                foreach($request->file('attachment') as $image)
                {
                    $name=time().'.'.$image->getClientOriginalName();
                    $destinationPath = 'share_file';
                    $image->move($destinationPath, $name);
                    $data[] = $name;
                    $insert=DB::table('tb_file_sharing')->insert([
                        'shared_group_type' =>$request->type_id,
                        'referenceId' =>$request->designation_id,
                        'shared_date' =>date('Y-m-d'),
                        'attachment' =>$name,
                        'designation_id' =>$request->designation_id,
                        'shared_by' =>auth()->user()->id,
                        'created_at'=>Carbon::now()->toDateTimeString(),
                        'updated_at'=>Carbon::now()->toDateTimeString()
                    ]);

                }
                return response()->json(['success' => 'File has been share successfully']);
            }
        }

    }

    //file share list admin wise
    public function fileShareListadmin(){
      $shareList=DB::table('tb_file_sharing')
      ->where('shared_by',auth()->user()->id)
      ->leftjoin('tb_employee','tb_file_sharing.emp_id','=','tb_employee.id')
      ->leftjoin('tb_branch','tb_file_sharing.branch_id','=','tb_branch.id')
      ->leftjoin('tb_departments','tb_file_sharing.department_id','=','tb_departments.id')
      ->leftjoin('tb_designations','tb_file_sharing.designation_id','=','tb_designations.id')
      ->select('tb_file_sharing.*','tb_file_sharing.id as main_id','tb_file_sharing.shared_date','tb_employee.employeeId','tb_employee.emp_first_name','tb_employee.emp_lastName','tb_branch.branch_name','tb_departments.department_name','tb_designations.designation_name')
      ->groupBy('tb_file_sharing.branch_id','tb_file_sharing.department_id','tb_file_sharing.designation_id','tb_file_sharing.emp_id')
      ->orderBy('tb_file_sharing.shared_group_type','DESC')
      ->get();
      if(request()->ajax())
      {
          return datatables()->of($shareList)
              ->addColumn('attachment', function($data){
                  if ($data->attachment != null) {
                      $asset = asset('share_file').'/'.$data->attachment ;
                      $button = '<a href="'.$asset.'" target="_blank"><span style="color:green">Open</span><i class="fa fa-download pull-right" aria-hidden="true"></i></a>';
                      $button .= '&nbsp;&nbsp;';
                  return $button;
                  }else {
                      return '<span style="color:red">No Attachment</span>';
                  }

              })
              ->addColumn('emp_details', function($shareList){

              })


              ->addColumn('action_btn', function($shareList){
                if($shareList->branch_id){
                    return '<a target="_blank" class="profile btn btn-blue btn-xs" href="'.url('/admin/wise/file/share/details/'.base64_encode($shareList->shared_group_type).'/'.base64_encode($shareList->branch_id)).'"><i class="fa fa-tasks"></i> View File</a>';
                }
                if($shareList->department_id){
                    return '<a target="_blank" class="profile btn btn-blue btn-xs" href="'.url('/admin/wise/file/share/details/'.base64_encode($shareList->shared_group_type).'/'.base64_encode($shareList->department_id)).'"><i class="fa fa-joomla"></i> View File</a>';
                }
                if($shareList->designation_id){
                    return '<a target="_blank" class="profile btn btn-blue btn-xs" href="'.url('/admin/wise/file/share/details/'.base64_encode($shareList->shared_group_type).'/'.base64_encode($shareList->designation_id)).'"><i class="fa fa-list-ul"></i> View File</a>';
                }
                if($shareList->emp_id){
                    return '<a target="_blank" class="profile btn btn-blue btn-xs" href="'.url('/admin/wise/file/share/details/'.base64_encode($shareList->shared_group_type).'/'.base64_encode($shareList->emp_id)).'"><i class="fa fa-users"></i> View File</a> <a target="_blank" class="profile btn btn-blue btn-xs" href="'.url('/employee/profile/'.base64_encode($shareList->emp_id)).'"><i class="fa fa-users"></i> View Profile</a>';

                }
              })
              ->rawColumns(['action','attachment','action_btn','emp_details'])
              ->addIndexColumn()
              ->make(true);
      }
      return view('backend.file_share.file_share_list');

    }

    //file share details
    public function fileShareListadminDetails($group,$id){
      $group=base64_decode($group);
      $id=base64_decode($id);
      if($group==1){
        $data=DB::table('tb_file_sharing')->where('emp_id',$id)
        ->leftjoin('tb_employee','tb_file_sharing.emp_id','=','tb_employee.id')
        ->select('tb_file_sharing.emp_id','tb_file_sharing.id as main_id','tb_file_sharing.attachment','tb_file_sharing.shared_date','tb_employee.employeeId','tb_employee.emp_first_name','tb_employee.emp_lastName')
        ->get();
        $employee=DB::table('tb_employee')->where('id',$id)->get();
        return view('backend.file_share.file_share_details',compact('data','employee'));
      }

      if($group==2){
        $data=DB::table('tb_file_sharing')->where('emp_id',$id)
        ->leftjoin('tb_employee','tb_file_sharing.emp_id','=','tb_employee.id')
        ->select('tb_file_sharing.emp_id','tb_file_sharing.id as main_id','tb_file_sharing.attachment','tb_file_sharing.shared_date','tb_employee.employeeId','tb_employee.emp_first_name','tb_employee.emp_lastName')
        ->get();
        $employee=DB::table('tb_employee')->where('id',$id)->get();
        return view('backend.file_share.file_share_details',compact('data','employee'));
      }

      if($group==3){
        $data=DB::table('tb_file_sharing')->where('branch_id',$id)
        ->leftjoin('tb_branch','tb_file_sharing.branch_id','=','tb_branch.id')
        ->select('tb_file_sharing.id as main_id','tb_file_sharing.attachment','tb_file_sharing.shared_date','tb_branch.branch_name')
        ->get();
        $employee=DB::table('tb_employee')->where('branch_id',$id)->get();
        return view('backend.file_share.file_share_details',compact('data','employee'));
      }

      if($group==4){
        $data=DB::table('tb_file_sharing')->where('department_id',$id)
        ->leftjoin('tb_departments','tb_file_sharing.department_id','=','tb_departments.id')
        ->select('tb_file_sharing.id as main_id','tb_file_sharing.attachment','tb_file_sharing.shared_date','tb_departments.department_name')
        ->get();
        $employee=DB::table('tb_employee')->where('emp_department_id',$id)->get();
        return view('backend.file_share.file_share_details',compact('data','employee'));
      }

      if($group==5){
        $data=DB::table('tb_file_sharing')->where('designation_id',$id)
        ->leftjoin('tb_designations','tb_file_sharing.designation_id','=','tb_designations.id')
        ->select('tb_file_sharing.id as main_id','tb_file_sharing.attachment','tb_file_sharing.shared_date','tb_designations.designation_name')
        ->get();
        $employee=DB::table('tb_employee')->where('emp_designation_id',$id)->get();
        return view('backend.file_share.file_share_details',compact('data','employee'));
      }

    }

    //file delete
    public function fileShareListadminDelete($id){
      $id=base64_decode($id);
      $file=DB::table('tb_file_sharing')->where('id',$id)->select('attachment')->first();
      $file_path="share_file"."/".$file->attachment;
      if(file_exists($file_path)){
        unlink($file_path);
      }
      DB::table('tb_file_sharing')->where(['id'=>$id])->delete();
      Session::flash('success','File has been deleted successfully');
      return redirect()->back();
    }

}
