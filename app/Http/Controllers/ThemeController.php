<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Repositories\Settings;
use Illuminate\Support\Facades\Session;

class ThemeController extends Controller
{
    public function theme_list(){

        $this->checkuserRole(['admin','super-admin','branch-manager'],'');
        return view('backend.theme_style.theme_list');
    }
    public function changeStyle(Request $request){
        $user=Auth::user();

        $str=DB::table('users')->where(['id'=>$user->id])->update([
            'theme_style' => $request->themeStyle,
        ]);

        if($str>0)
        {
            Session::flash('sweetMessage','Theme Style has been changed. Please refresh(press ctrl+f5 to refresh ) your browser if is it not rendering currectly. ');
        }else{
            Session::flash('sweetFailedMessage','Failed to change theme style. Try again later...');
        }
        // return $str;
        return redirect()->back();
    }


    public function language_selector(){
        return view('backend.language.language_selector');
    }

    public function change_language_selector(Request $request){        
        $user=Auth::user();

        $str=DB::table('users')->where(['id'=>$user->id])->update([
            'system_language' => $request->languageSelector,
        ]);

        if($str>0)
        {
            Session::put('locale',  $request->languageSelector);
            Session::flash('sweetMessage','System Language has been changed. Please refresh(press ctrl+f5 to refresh ) your browser if is it not rendering currectly. ');
        }else{
            Session::flash('sweetFailedMessage','Failed to change language. Try again later...');
        }

        return redirect()->back();
    }


}
