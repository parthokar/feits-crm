<?php

namespace App\Http\Controllers;

use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicle_list=DB::table('tb_vehicle_info')
        ->leftJoin('users','users.id','=','tb_vehicle_info.created_by')
        ->leftJoin('tb_branch','tb_branch.id','=','tb_vehicle_info.branch_id')
        ->select('tb_vehicle_info.*','users.name as created_by_name', 'tb_branch.branch_name as biBranchName')
        ->get();
        // dd($vehicle_list);
        if(request()->ajax())
        {
            return datatables()->of($vehicle_list)
                ->addColumn('action', function($data){
                    $button="";

                    // $button .= '<button type="button" name="edit" id="'.$data->id.'" class="edit btn btn-blue btn-xs" data-toggle="modal" data-target="#editBookingItem" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button>&nbsp;&nbsp;';

                    if($data->status==1){
                        $button .= '<a href="#" onclick="confirmInactive('.$data->id.')" class="btn btn-warning btn-xs" data-placement="top" title="Deactivate this item"><i class="fa fa-ban"></i></a>&nbsp;&nbsp;';
                    }

                    if($data->status==0){
                        $button .= '<a href="#" onclick="confirmActive('.$data->id.')" class="btn btn-info btn-xs" data-placement="top" title="Activate this item"><i class="fa fa-check"></i></a>&nbsp;&nbsp;';
                    }

                    $button .= '<a href="#" onclick="confirmDelete('.$data->id.')" class="btn btn-danger btn-xs" data-placement="top" title="Move to trash"><i class="fa fa-trash-o"></i></a>';
                    $button .= '&nbsp;&nbsp;';
                    
                    return $button;
                })
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }

        return view('backend.vehicle_booking.vehicle_list');
    } 

   
    public function store(Request $request)
    {
        $rules = array(
            'branch_id'=>'required',
            'vehicle_name'=>'required'
        );
  
        $messages = array(
            'branch_id.required' => 'Branch is required.',
            'vehicle_name.required' => 'Vehicle Name is required.'
        );

        $error = Validator::make($request->all(), $rules, $messages);
        if($error->fails())
        {
            return response()->json(['errors' => $error->errors()->all()]);
        }

        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();

        $str=DB::table('tb_vehicle_info')->insert([
            'vehicle_name'=>$request->vehicle_name,
            'engine_machine_number'=>$request->engine_machine_number,
            'registration_number'=>$request->registration_number,
            'insurance_number'=>$request->insurance_number,
            'remarks'=>$request->remarks,
            'branch_id'=>$request->branch_id,
            'created_by'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
        
        if ($str) {
            return response()->json(['title' => 'Success!', 'message' => 'New vehicle information has been successfully added. !', 'icon' => 'success']);
         } else {
            return response()->json(['title' => 'Failed!', 'message' => 'Insertion has been failed', 'icon' => 'error']);
         }
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'vehicleName'=>'required',
        ]);
        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();

        $des=DB::table('tbvehicle_info')->where('id','=',$id)->update([
            'vehicleName'=>$request->vehicleName,
            'registrationNumber'=>$request->registrationNumber,
            'insuranceNumber'=>$request->insuranceNumber,
            'createdBy'=>$user->id,
            'updated_at'=>$now,

        ]);
        
        if($des){
            Session::flash('message','Vehicle Updated Successfully');
            return  redirect()->back();
        }
    }


    public function destroy_vehicle_information($id)
    {
       $check=DB::table('tb_vehicle_expenses')->where(['vehicle_id'=>$id])->get();
        if(count($check)){
            return response()->json(['title' => 'Deletion failed!', 'message' => 'There are already some data bearing this resource.', 'icon' => 'error']);
        }
        else {
           $str = DB::table('tb_vehicle_info')->where(['id'=>$id])->delete();

            if($str){
                return response()->json(['title' => 'Deleted!', 'message' => 'Vehicle information has been successfully deleted. !', 'icon' => 'success']);
            }else{
                return response()->json(['title' => 'Failed!', 'message' => 'Deletion failed. !', 'icon' => 'error']);
            }
        }
    }


    public function active_vehicle($id)
    {
        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();

        $str = DB::table('tb_vehicle_info')->where(['id'=>$id])->update([
            'status'=>1,
            'created_by'=>$user->id,
            'updated_at'=>$now,
        ]);

       if($str){
            return response()->json(['title' => 'Activated!', 'message' => 'Selected Vehicle has been successfully activated. !', 'icon' => 'success']);
        }else{
            return response()->json(['title' => 'Failed!', 'message' => 'Activation failed. !', 'icon' => 'error']);
        }
    }

    public function inactive_vehicle($id)
    {

        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();

        $str = DB::table('tb_vehicle_info')->where(['id'=>$id])->update([
            'status'=>0,
            'created_by'=>$user->id,
            'updated_at'=>$now,
        ]);

      
       if($str){
            return response()->json(['title' => 'Deactivated!', 'message' => 'Selected Vehicle has been successfully deactivated. !', 'icon' => 'success']);
        }else{
            return response()->json(['title' => 'Failed!', 'message' => 'Deactivation failed. !', 'icon' => 'error']);
        }
    }



    public function new_booking_request()
    {
        return view('backend.vehicle_booking.new_booking_request');
    }
    
    
    public function get_vehicle_list_by_branch_id($id)
    {
        $vehicle_list=DB::table('tb_vehicle_info')->where([['status','=', 1],['branch_id','=', $id]])->orderBy('vehicle_name','ASC')->get();
        return view('backend.ajax.get_vehicle_list_by_branch_id',compact('vehicle_list'));
    }

    public function store_booking(Request $request)
    {
        $rules = array(
            'vehicle_id' => 'required',
            'booked_by' => 'required',
            'driver_name' => 'required',
            'vb_date' => 'required',
            'vb_startTime' => 'required',
            'vb_EndTime' => 'required'
        );
  
        $messages = array(
            'vehicle_id.required' => 'Vehicle is required.',
            'booked_by.required' => 'Booked By is required.',
            'driver_name.required' => 'Driver name is required.',
            'vb_date.required' => 'Booking date is required.',
            'vb_startTime.required' => 'Booking starting time is required.',
            'vb_EndTime.required' => 'Booking ending time is required.'
        );
        
        $error = Validator::make($request->all(), $rules, $messages);
        if($error->fails())
        {
            return response()->json(['errors' => $error->errors()->all()]);
        }

        $save=0;
        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();

        $str = DB::table('tb_vehicle_booking')->where([['vehicle_id','=',$request->vehicle_id],['status','=',1],['vb_date','=',Carbon::parse($request->vb_date)->format('Y-m-d')]])->get();

        foreach ($str as $key) {
            $time1=strtotime(Carbon::parse($key->vb_startTime)->format('H:i:s')); 
            $time2=strtotime(Carbon::parse($key->vb_EndTime)->format('H:i:s')); 
            $currenttime=strtotime((Carbon::parse($request->vb_startTime)->format('H:i:s'))); 

            if($time1 - $currenttime <= 0 && $currenttime - $time2 <= 0 ) { 
                $save++;
            }
        }

        if($save==0){
            $str = DB::table('tb_vehicle_booking')->insert([
                'vehicle_id'=>$request->vehicle_id,
                'booked_by'=>$request->booked_by,
                'driver_name'=>$request->driver_name,
                'vb_date'=>Carbon::parse($request->vb_date)->format('Y-m-d'),
                'vb_startTime'=>$request->vb_startTime,
                'vb_EndTime'=>$request->vb_EndTime,
                'purpose'=>$request->purpose,
                'created_by'=>$user->id,
                'status'=>1,
                'created_at'=>$now,
                'updated_at'=>$now,
            ]);
            return response()->json(['title' => 'Success!', 'message' => 'Vehicle booking request information has been successfully submitted. !', 'icon' => 'success']);
        }else{
            return response()->json(['title' => 'Failed!', 'message' => 'This vehicle already booked for selected Date/time. Choose another vehicle or choose different date/time.!', 'icon' => 'error']);
        }
    }

    public function booking_list()
    {
        $date=date('Y-m-d');
        $booking_history=DB::table('tb_vehicle_booking')
        ->leftJoin('tb_vehicle_info','tb_vehicle_info.id','=','tb_vehicle_booking.vehicle_id')
        ->leftJoin('tb_branch','tb_branch.id','=','tb_vehicle_info.branch_id')
        ->leftJoin('users','users.id','=','tb_vehicle_booking.created_by')
        ->select('tb_vehicle_booking.*','tb_vehicle_info.vehicle_name as vehicleName','users.name as createdBy', 'tb_branch.branch_name as branchName' )
        ->whereDate('vb_date', '>=', $date)
        ->get();
        // dd($booking_history);
        if(request()->ajax())
        {
            return datatables()->of($booking_history)
                    ->addColumn('vbDate', function($data){
                        return date('d-m-Y', strtotime($data->vb_date));
                    })
                    ->addColumn('action', function($data){
                        $button="";
                        if($data->status==1){
                            $button .= '<a href="#" onclick="confirmCancel('.$data->id.')" class="btn btn-warning btn-xs" data-placement="top" title="Cancel booking request"><i class="fa fa-ban"></i></a>&nbsp;&nbsp;';
                        }

                        if($data->status==0){
                            $button .= '<a href="#" onclick="confirmActive('.$data->id.')" class="btn btn-info btn-xs" data-placement="top" title="Activate booking request"><i class="fa fa-check"></i></a>&nbsp;&nbsp;';
                        }

                        $button .= '<a href="#" onclick="confirmDelete('.$data->id.')" class="btn btn-danger btn-xs" data-placement="top" title="Move to trash"><i class="fa fa-trash-o"></i></a>';
                        $button .= '&nbsp;&nbsp;';
                        
                        return $button;
                    })
                    ->rawColumns(['action'])
                    ->addIndexColumn()
                    ->make(true);
        }
        // dd($booking_history);
        return view('backend.vehicle_booking.booking_list');
    }


    public function booking_cancel($id)
    {
        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();

        $str = DB::table('tb_vehicle_booking')->where(['id'=>$id])->update([
            'status'=>0,
            'created_by'=>$user->id,
            'updated_at'=>$now,
        ]);
      
       if($str){
            return response()->json(['title' => 'Cancelled!', 'message' => 'Vehicle Booking request has been successfully cancelled. !', 'icon' => 'success']);
        }else{
            return response()->json(['title' => 'Failed!', 'message' => 'Cancellation failed. !', 'icon' => 'error']);
        }
    }

    public function booking_request_active($id)
    {
        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();

        $str = DB::table('tb_vehicle_booking')->where(['id'=>$id])->update([
            'status'=>1,
            'created_by'=>$user->id,
            'updated_at'=>$now,
        ]);

       if($str){
            return response()->json(['title' => 'Activated!', 'message' => 'Vehicle Booking request has been successfully activated. !', 'icon' => 'success']);
        }else{
            return response()->json(['title' => 'Failed!', 'message' => 'Activation failed. !', 'icon' => 'error']);
        }
    }


    public function booking_delete($id)
    {
        $str = DB::table('tb_vehicle_booking')->where(['id'=>$id])->delete();

       if($str){
            return response()->json(['title' => 'Deleted!', 'message' => 'Vehicle Booking request has been successfully deleted. !', 'icon' => 'success']);
        }else{
            return response()->json(['title' => 'Failed!', 'message' => 'Deletion failed. !', 'icon' => 'error']);
        }
    }


    public function expense_category_list()
    {
        $expenses_category=DB::table('tb_vexcategory')
            ->leftJoin('users','users.id','=','tb_vexcategory.created_by')
            ->select('tb_vexcategory.*', 'users.name as createdBy')
            ->get();

        if(request()->ajax())
        {
            return datatables()->of($expenses_category)
                ->addColumn('action', function($data){
                    $button="";
                    if($data->status==1){
                        $button .= '<a href="#" onclick="confirmInactive('.$data->id.')" class="btn btn-warning btn-xs" data-placement="top" title="Inactive"><i class="fa fa-ban"></i></a>&nbsp;&nbsp;';
                    }

                    if($data->status==0){
                        $button .= '<a href="#" onclick="confirmActive('.$data->id.')" class="btn btn-info btn-xs" data-placement="top" title="Active"><i class="fa fa-check"></i></a>&nbsp;&nbsp;';
                    }

                    $button .= '<a href="#" onclick="confirmDelete('.$data->id.')" class="btn btn-danger btn-xs" data-placement="top" title="Move to trash"><i class="fa fa-trash-o"></i></a>';
                    $button .= '&nbsp;&nbsp;';
                    
                    return $button;
                })
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
        return view('backend.vehicle_booking.expense_category_list');
    }


    public function active_expense_category($id)
    {
        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();

        $str = DB::table('tb_vexcategory')->where(['id'=>$id])->update([
            'status'=>1,
            'created_by'=>$user->id,
            'updated_at'=>$now,
        ]);

       if($str){
            return response()->json(['title' => 'Activated!', 'message' => 'Selected Category has been successfully activated. !', 'icon' => 'success']);
        }else{
            return response()->json(['title' => 'Failed!', 'message' => 'Activation failed. !', 'icon' => 'error']);
        }
    }

    public function inactive_expense_category($id)
    {

        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();

        $str = DB::table('tb_vexcategory')->where(['id'=>$id])->update([
            'status'=>0,
            'created_by'=>$user->id,
            'updated_at'=>$now,
        ]);

      
       if($str){
            return response()->json(['title' => 'Deactivated!', 'message' => 'Selected Category has been successfully deactivated. !', 'icon' => 'success']);
        }else{
            return response()->json(['title' => 'Failed!', 'message' => 'Deactivation failed. !', 'icon' => 'error']);
        }
    }

    public function delete_expense_category($id)
    {
       $check=DB::table('tb_vehicle_expenses')->where(['vex_category_id'=>$id])->get();
        if(count($check)){
            return response()->json(['title' => 'Failed!', 'message' => 'There are already some data bearing this resource.', 'icon' => 'error']);
        }
        else {
           $str = DB::table('tb_vexcategory')->where(['id'=>$id])->delete();

            if($str){
                return response()->json(['title' => 'Deleted!', 'message' => 'Expense Category has been successfully deleted. !', 'icon' => 'success']);
            }else{
                return response()->json(['title' => 'Failed!', 'message' => 'Deletion failed. !', 'icon' => 'error']);
            }
        }
    }

    public function expense_category_store(Request $request)
    {
        $rules = array(
            'vcategory_name' => 'required|unique:tb_vexcategory'
        );
  
        $messages = array(
            'vcategory_name.required' => 'Expense category is required.'
        );
        
        $error = Validator::make($request->all(), $rules, $messages);
        if($error->fails())
        {
            return response()->json(['errors' => $error->errors()->all()]);
        }

        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();

        $str=DB::table('tb_vexcategory')->insert([
            'vcategory_name' => $request->vcategory_name,
            'vcat_description' => $request->vcat_description,
            'created_by' => $user->id,
            'status' => 1,
            'created_at' => $now,
            'updated_at' => $now,

        ]);
        if($str){
             return response()->json(['title' => 'Success!', 'message' => 'Expense category information has been successfully saved. !', 'icon' => 'success']);
        }else{
            return response()->json(['title' => 'Failed!', 'message' => 'Something went wrong. ', 'icon' => 'error']);
        }
    }

    public function excatupdate(Request $request)
    {
        $this->validate($request,[
            'vexName'=>'required',
        ]);
        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();

        $des=DB::table('tbvexcategory')->where(['id'=>$request->id])->update([
            'vexName'=>$request->vexName,
            'vexDescription'=>$request->vexDescription,
            'createdBy'=>$user->id,
            'updated_at'=>$now,

        ]);
        if($des){
            Session::flash('message','Vehicle expense category successfully updated.');
            return  redirect()->back();
        }
    }

    public function expenses()
    {
        
        $vfuel_expense=DB::table('tbvfuel_expense')
        ->leftJoin('tbvehicle_info','tbvehicle_info.id','=','tbvfuel_expense.vehicleId')
        ->get();

        $vehicle_expenses=DB::table('tbvehicle_expenses')
        ->leftJoin('tbvehicle_info','tbvehicle_info.id','=','tbvehicle_expenses.vehicleId')
        ->leftJoin('tbvexcategory','tbvexcategory.id','=','tbvehicle_expenses.vexCategoryId')
        ->select('tbvehicle_expenses.*','tbvehicle_info.vehicleName as vehicleName','tbvexcategory.vexName as vexName')
        ->get();

        $vehicle_info=DB::table('tbvehicle_info')->get();
        $vexcategory=DB::table('tbvexcategory')->get();

        return view('vehicle.expenses',compact('vfuel_expense','vehicle_expenses','vehicle_info','vexcategory'));
    }

    public function newfuelexpense(Request $request){
        $this->validate($request,[
            'vehicleId'=>'required',
            'fuelType'=>'required',
            'amount'=>'required',
            'vexDate'=>'required',
        ]);

        if($file=$request->file('newfuelAttachment')){
            if($request->file('newfuelAttachment')->getClientSize()>5000000)
            {
                Session::flash('fileSize','Could Not Upload. File Size Limit Exceeded.');
                return redirect()->back();
            }
            $name=time()."_"."fe_".$file->getClientOriginalName();
            $file->move('vehicle_expenses',$name);
        }
        else{
            $name=null;
        }

        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();
        $str = DB::table('tbvfuel_expense')->insert([
            'vehicleId'=>$request->vehicleId,
            'fuelType'=>$request->fuelType,
            'vexReferences'=>$request->vexReferences,
            'vexDate'=>Carbon::parse($request->vexDate)->format('Y-m-d'),
            'quantity'=>$request->quantity,
            'amount'=>$request->amount,
            'vexDescription'=>$request->vexDescription,
            'vexAttachment'=>$name,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);

       if($str)
        {
            Session::flash('message','Fuel Expense Information Successfully Added. ');
        }else{
            Session::flash('failedMessage','Fuel Expense Information Adding Failed.');
        }
        return redirect()->back();
    }

    public function newotherexpense (Request $request){
        $this->validate($request,[
            'vehicleId'=>'required',
            'vexCategoryId'=>'required',
            'amount'=>'required',
        ]);

        if($file=$request->file('newOtherAttachment')){
            if($request->file('newOtherAttachment')->getClientSize()>5000000)
            {
                Session::flash('fileSize','Could Not Upload. File Size Limit Exceeded.');
                return redirect()->back();

            }
            $name=time()."_"."oe_".$file->getClientOriginalName();
            $file->move('vehicle_expenses',$name);
        }
        else{
            $name=null;
        }

        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();
        $str = DB::table('tbvehicle_expenses')->insert([
            'vehicleId'=>$request->vehicleId,
            'vexCategoryId'=>$request->vexCategoryId,
            'amount'=>$request->amount,
            'vexDate'=>Carbon::parse($request->vexDate)->format('Y-m-d'),
            'vexReferences'=>$request->vexReferences,
            'vexDescription'=>$request->vexDescription,
            'vexAttachment'=>$name,
            'createdBy'=>$user->id,
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);

       if($str)
        {
            Session::flash('message','Expense Information Successfully Added. ');
        }else{
            Session::flash('failedMessage','Expense Information Adding Failed.');
        }
        return redirect()->back();
    }

    public function update_booking(Request $request)
    {
        $this->validate($request,[
            'vehicleId'=>'required',
            'bookedBy'=>'required',
            'driverName'=>'required',
            'vbDate'=>'required',
        ]);

        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();

        $str = DB::table('tbvehicle_booking')->where(['id'=>$request->id])->update([
            'vehicleId'=>$request->vehicleId,
            'bookedBy'=>$request->bookedBy,
            'driverName'=>$request->driverName,
            'vbDate'=>Carbon::parse($request->vbDate)->format('Y-m-d'),
            'vbStartTime'=>$request->vbStartTime,
            'vbEndTime'=>$request->vbEndTime,
            'purpose'=>$request->purpose,
            'createdBy'=>$user->id,
            'updated_at'=>$now,
        ]);

       if($str)
        {
            Session::flash('message','Vehicle Booking Information Successfully Updated ');
        }else{
            Session::flash('failedMessage','Vehicle Booking Information Updation Failed.');
        }
        return redirect()->back();
    }


    public function report_view()
    {
        return view('vehicle.report_view');
    }


    public function vehicle_wise_report_view()
    {
        $vehicle_info=DB::table('tbvehicle_info')->get();
        return view('vehicle.vehicle_wise_report_view',compact('vehicle_info'));
    }

    public function vehicle_wise_report_data(Request $request)
    {
        $start_date=Carbon::parse($request->start_date)->format('Y-m-d');
        $end_date=Carbon::parse($request->end_date)->format('Y-m-d');
       if(($request->vehicleId)==0){
            $vfuel_expense=DB::table('tbvfuel_expense')
            ->select('tbvfuel_expense.*','tbvehicle_info.vehicleName as vehicleName','tbvehicle_info.registrationNumber as registrationNumber')
            ->leftJoin('tbvehicle_info','tbvehicle_info.id','=','tbvfuel_expense.vehicleId')
            ->whereBetween('tbvfuel_expense.vexDate', [$start_date, $end_date])
            ->get();
            $vehicle_expenses=DB::table('tbvehicle_expenses')
            ->leftJoin('tbvehicle_info','tbvehicle_info.id','=','tbvehicle_expenses.vehicleId')
            ->leftJoin('tbvexcategory','tbvexcategory.id','=','tbvehicle_expenses.vexCategoryId')
            ->select('tbvehicle_expenses.*','tbvehicle_info.vehicleName as vehicleName','tbvehicle_info.registrationNumber as registrationNumber','tbvexcategory.vexName as vexName')
            ->whereBetween('tbvehicle_expenses.vexDate', [$start_date, $end_date])
            ->get();
       }else{
            $vfuel_expense=DB::table('tbvfuel_expense')
            ->leftJoin('tbvehicle_info','tbvehicle_info.id','=','tbvfuel_expense.vehicleId')
            ->select('tbvfuel_expense.*','tbvehicle_info.vehicleName as vehicleName','tbvehicle_info.registrationNumber as registrationNumber')
            ->whereBetween('tbvfuel_expense.vexDate', [$start_date, $end_date])
            ->where('tbvfuel_expense.vehicleId','=',$request->vehicleId)
            ->get();

            $vehicle_expenses=DB::table('tbvehicle_expenses')
            ->leftJoin('tbvehicle_info','tbvehicle_info.id','=','tbvehicle_expenses.vehicleId')
            ->leftJoin('tbvexcategory','tbvexcategory.id','=','tbvehicle_expenses.vexCategoryId')
            ->select('tbvehicle_expenses.*','tbvehicle_info.vehicleName as vehicleName','tbvehicle_info.registrationNumber as registrationNumber','tbvexcategory.vexName as vexName')
            ->whereBetween('tbvehicle_expenses.vexDate', [$start_date, $end_date])
            ->where('tbvehicle_expenses.vehicleId','=',$request->vehicleId)
            ->get();
       }
        return view('vehicle.vehicle_expense_report_data',compact('vfuel_expense','vehicle_expenses','request'));
    }

    public function vehicle_booking_report_view()
    {
        return view('backend.vehicle_booking.vehicle_booking_report_view');
    }

    public function vehicle_booking_report_data(Request $request)
    {
        $start_date=Carbon::parse($request->start_date)->format('Y-m-d');
        $end_date=Carbon::parse($request->end_date)->format('Y-m-d');
       if(empty($request->vehicleId)){
            $booking_list=DB::table('tb_vehicle_booking')
            ->leftJoin('tb_vehicle_info','tb_vehicle_info.id','=','tb_vehicle_booking.vehicle_id')
            ->leftJoin('tb_branch','tb_branch.id','=','tb_vehicle_info.branch_id')
            ->select('tb_vehicle_booking.*','tb_vehicle_info.vehicle_name','tb_vehicle_info.registration_number as registrationNumber','tb_branch.branch_name as branchName')
            ->whereBetween('tb_vehicle_booking.vb_date', [$start_date, $end_date])
            ->where('tb_vehicle_info.branch_id','=',$request->branch_id)
            ->orderBy('vb_date', 'ASC')
            ->get();
       }else{
            $booking_list=DB::table('tb_vehicle_booking')
            ->leftJoin('tb_vehicle_info','tb_vehicle_info.id','=','tb_vehicle_booking.vehicleId')
            ->leftJoin('tb_branch','tb_branch.id','=','tb_vehicle_info.branch_id')
            ->select('tb_vehicle_booking.*','tb_vehicle_info.vehicle_name','tb_vehicle_info.registration_number as registrationNumber','tb_branch.branch_name as branchName')
            ->whereBetween('tb_vehicle_booking.vb_date', [$start_date, $end_date])
            ->where('tb_vehicle_booking.vehicle_id','=',$request->vehicle_id)
            ->orderBy('vb_date', 'ASC')
            ->get();

       }

       // dd($booking_list);
        return view('backend.vehicle_booking.vehicle_booking_report_data',compact('booking_list','request'));
    }



}
