<?php

namespace App\Http\Controllers;

use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // All Booking Items List
    public function index()
    {
        $booking_items=DB::table('tb_booking_items')
        ->leftJoin('users','users.id','=','tb_booking_items.createdBy')
        ->leftJoin('tb_branch','tb_branch.id','=','tb_booking_items.biBranchId')
        ->select('tb_booking_items.*','users.name as created_by', 'tb_branch.branch_name as biBranchName')
        ->get();
        // dd($booking_items);
        if(request()->ajax())
        {
            return datatables()->of($booking_items)
                    ->addColumn('action', function($data){
                        $button="";

                        $button .= '<button type="button" name="edit" id="'.$data->id.'" class="edit btn btn-blue btn-xs" data-toggle="modal" data-target="#editBookingItem" data-placement="top" title="Edit"><i class="fa fa-edit"></i></button>&nbsp;&nbsp;';

                        if($data->status==1){
                            $button .= '<a href="#" onclick="confirmInactive('.$data->id.')" class="btn btn-warning btn-xs" data-placement="top" title="Deactivate this item"><i class="fa fa-ban"></i></a>&nbsp;&nbsp;';
                        }

                        if($data->status==0){
                            $button .= '<a href="#" onclick="confirmActive('.$data->id.')" class="btn btn-info btn-xs" data-placement="top" title="Activate this item"><i class="fa fa-check"></i></a>&nbsp;&nbsp;';
                        }

                        $button .= '<a href="#" onclick="confirmDelete('.$data->id.')" class="btn btn-danger btn-xs" data-placement="top" title="Move to trash"><i class="fa fa-trash-o"></i></a>';
                        $button .= '&nbsp;&nbsp;';
                        
                        return $button;
                    })
                    ->rawColumns(['action'])
                    ->addIndexColumn()
                    ->make(true);
        }

        return view('backend.booking.bookingitems');
    } 


    public function active_booking_item($id)
    {

        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();

        $str = DB::table('tb_booking_items')->where(['id'=>$id])->update([
            'status'=>1,
            'createdBy'=>$user->id,
            'updated_at'=>$now,
        ]);

      
       if($str){
            return response()->json(['title' => 'Activated!', 'message' => 'Booking item has been successfully activated. !', 'icon' => 'success']);
        }else{
            return response()->json(['title' => 'Failed!', 'message' => 'Activation failed. !', 'icon' => 'error']);
        }
    }

    public function inactive_booking_item($id)
    {

        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();

        $str = DB::table('tb_booking_items')->where(['id'=>$id])->update([
            'status'=>0,
            'createdBy'=>$user->id,
            'updated_at'=>$now,
        ]);

      
       if($str){
            return response()->json(['title' => 'Deactivated!', 'message' => 'Booking item has been successfully deactivated. !', 'icon' => 'success']);
        }else{
            return response()->json(['title' => 'Failed!', 'message' => 'Deactivation failed. !', 'icon' => 'error']);
        }
    }


    public function bookinglist()
    {
        $date=date('Y-m-d');
        $booking_history=DB::table('tb_booking_history')
        ->leftJoin('tb_booking_items','tb_booking_items.id','=','tb_booking_history.biId')
        ->leftJoin('tb_branch','tb_branch.id','=','tb_booking_items.biBranchId')
        ->leftJoin('users','users.id','=','tb_booking_history.createdBy')
        ->select('tb_booking_history.*','tb_booking_items.biName','users.name as created_by', 'tb_branch.branch_name as biBranchName' )
        ->whereDate('biDate', '>=', $date)
        ->get();

        if(request()->ajax())
        {
            return datatables()->of($booking_history)
                    ->addColumn('biBDate', function($data){
                        return date('d-m-Y', strtotime($data->biDate));
                    })
                    ->addColumn('action', function($data){
                        $button="";
                        if($data->status==1){
                            $button .= '<a href="#" onclick="confirmCancel('.$data->id.')" class="btn btn-warning btn-xs" data-placement="top" title="Cancel booking request"><i class="fa fa-ban"></i></a>&nbsp;&nbsp;';
                        }

                        if($data->status==0){
                            $button .= '<a href="#" onclick="confirmActive('.$data->id.')" class="btn btn-info btn-xs" data-placement="top" title="Active booking request"><i class="fa fa-check"></i></a>&nbsp;&nbsp;';
                        }

                        $button .= '<a href="#" onclick="confirmDelete('.$data->id.')" class="btn btn-danger btn-xs" data-placement="top" title="Move to trash"><i class="fa fa-trash-o"></i></a>';
                        $button .= '&nbsp;&nbsp;';
                        
                        return $button;
                    })
                    ->rawColumns(['action'])
                    ->addIndexColumn()
                    ->make(true);
        }
        // dd($booking_history);
        return view('backend.booking.booking_list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'biName'=>'required|unique:tb_booking_items',
        );

        $error = Validator::make($request->all(), $rules);
        if($error->fails())
        {
            return response()->json(['errors' => $error->errors()->all()]);
        }

        $str = DB::table('tb_booking_items')->insert([
            'biName'=>$request->biName,
            'biDescription'=>$request->biDescription,
            'status'=>1,
            'createdBy'=>Auth::user()->id,
            'created_at'=>Carbon::now()->toDateTimeString(),
            'updated_at'=>Carbon::now()->toDateTimeString()
        ]);

        if ($str) {
            return response()->json(['success' => 'Booking Item has been successfully added.']);
         } else {
            return 0;
         }
    }

    public function booking_item_edit($id)
    {
        $data = DB::table('tb_booking_items')->where('id',$id)->first(['id','biName','biDescription','status']);
        return response()->json($data);
    }



    public function update(Request $request)
    {
        
         $rules = array(
            'biName'=>'required',
        );

        $error = Validator::make($request->all(), $rules);
        if($error->fails())
        {
            return response()->json(['errors' => $error->errors()->all()]);
        }


        $str=DB::table('tb_booking_items')->where(['id'=>$request->id])->update([
            'biName'=>$request->biName,
            'biDescription'=>$request->biDescription,
            'status'=>$request->status,
            'createdBy'=>Auth::user()->id,
            'updated_at'=>Carbon::now()->toDateTimeString()
        ]);

       
        if ($str) {
            return response()->json(['success' => 'Booking Item has been successfully updated.']);
         } else {
            return 0;
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $check=DB::table('tbbooking_history')->where(['biId'=>$id])->get();
        if(count($check)){
            Session::flash('edit',"Can't delete. " );
        }
        else {
            DB::table('tbbooking_items')->where(['id'=>$id])->delete();
            Session::flash('message', "Successfully has been deleted.");
        }
        return redirect()->back();
    }    

    public function bookingitems_delete($id)
    {
        $check=DB::table('tb_booking_history')->where(['biId'=>$id])->get();
        if(count($check)){
                return response()->json(['title' => 'Deletion failed!', 'message' => 'There are already some data bearing this resource.', 'icon' => 'error']);
        }else{
            $str = DB::table('tb_booking_items')->where(['id'=>$id])->delete();

            if($str){
                return response()->json(['title' => 'Deleted!', 'message' => 'Booking item information has been successfully deleted. !', 'icon' => 'success']);
            }else{
                return response()->json(['title' => 'Failed!', 'message' => 'Deletion failed. !', 'icon' => 'error']);
            }
        }
    }

    // public function bookingitems()
    // {
    //     $booking_items=DB::table('tbbooking_items')->get();
    //     return view('booking.bookingitems',compact('booking_items'));
    // }

    public function new_request()
    {
        $booking_items=DB::table('tb_booking_items')->where('status','=', 1)->get();
        return view('backend.booking.new_booking_request',compact('booking_items'));
    }
    
    public function get_booking_item_by_branch_id($id)
    {
        $booking_items=DB::table('tb_booking_items')->where([['status','=', 1],['biBranchId','=', $id]])->orderBy('biName','ASC')->get();
        return view('backend.ajax.get_booking_item_by_branch_id',compact('booking_items'));
    }

    public function save_new_request(Request $request)
    {

        $rules = array(
            'biBranchId'=>'required',
            'biId'=>'required',
            'bookedBy'=>'required',
            'biDate'=>'required',
            'biStartTime'=>'required',
            'biEndTime'=>'required',
        );
  
        $messages = array(
            'biBranchId.required' => 'Branch Name is required.',
            'biId.required' => 'Booking item is required.',
            'bookedBy.required' => 'Booked by field is required.',
            'biDate.required' => 'Booking date field is required.',
            'biStartTime.required' => 'Booking starting time field is required.',
            'biEndTime.required' => 'Booking ending time field is required.'
        );

        $error = Validator::make($request->all(), $rules, $messages);
        if($error->fails())
        {
            return response()->json(['errors' => $error->errors()->all()]);
        }

        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();

        $str = DB::table('tb_booking_history')->where([['biId','=',$request->biId],['status','=',1],['biDate','=',Carbon::parse($request->biDate)->format('Y-m-d')]])->get();
        $save=0;


        foreach ($str as $key) {
            $time1=strtotime(Carbon::parse($key->biStartTime)->format('H:i:s')); 
            $time2=strtotime(Carbon::parse($key->biEndTime)->format('H:i:s')); 
            $currenttime=strtotime((Carbon::parse($request->biStartTime)->format('H:i:s'))); 

            if($time1 - $currenttime <= 0 && $currenttime - $time2 <= 0 ) { 
                $save++;
            }
        }

        if($save==0){
            $des=DB::table('tb_booking_history')->insert([
                'biId'=>$request->biId,
                'bookedBy'=>$request->bookedBy,
                'purpose'=>$request->purpose,
                'biDate'=>Carbon::parse($request->biDate)->format('Y-m-d'),
                'biStartTime'=>$request->biStartTime,
                'biEndTime'=>$request->biEndTime,
                'status'=>1,
                'createdBy'=>$user->id,
                'created_at'=>$now,
                'updated_at'=>$now,

            ]);

                return response()->json(['title' => 'Success!', 'message' => 'Booking request information has been successfully submitted. !', 'icon' => 'success']);
            }else{
                return response()->json(['title' => 'Failed!', 'message' => 'This Item already booked for selected Date/time. Choose another items/room or choose different date/time.!', 'icon' => 'error']);
            }
    }


    public function booking_cancel($id)
    {

        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();

        $str = DB::table('tb_booking_history')->where(['id'=>$id])->update([
            'status'=>0,
            'createdBy'=>$user->id,
            'updated_at'=>$now,
        ]);

      
       if($str){
            return response()->json(['title' => 'Cancelled!', 'message' => 'Booking request has been successfully cancelled. !', 'icon' => 'success']);
        }else{
            return response()->json(['title' => 'Failed!', 'message' => 'Cancellation failed. !', 'icon' => 'error']);
        }
    }

    public function booking_request_active($id)
    {

        $user=Auth::user();
        $now=Carbon::now()->toDateTimeString();

        $str = DB::table('tb_booking_history')->where(['id'=>$id])->update([
            'status'=>1,
            'createdBy'=>$user->id,
            'updated_at'=>$now,
        ]);

      
       if($str){
            return response()->json(['title' => 'Activated!', 'message' => 'Booking request has been successfully activated. !', 'icon' => 'success']);
        }else{
            return response()->json(['title' => 'Failed!', 'message' => 'Activation failed. !', 'icon' => 'error']);
        }
    }


    public function booking_delete($id)
    {
        $str = DB::table('tb_booking_history')->where(['id'=>$id])->delete();

       if($str){
            return response()->json(['title' => 'Deleted!', 'message' => 'Booking request has been successfully deleted. !', 'icon' => 'success']);
        }else{
            return response()->json(['title' => 'Failed!', 'message' => 'Deletion failed. !', 'icon' => 'error']);
        }
    }


    public function booking_report_view()
    {
        $booking_items=DB::table('tb_booking_items')->get();
        return view('backend.booking.date_wise_booking_report',compact('booking_items'));
    }

    public function booking_report_data(Request $request)
    {
        $start_date=Carbon::parse($request->start_date)->format('Y-m-d');
        $end_date=Carbon::parse($request->end_date)->format('Y-m-d');
       if(empty(($request->biId))){
            $booking_history=DB::table('tb_booking_history')
            ->leftJoin('tb_booking_items','tb_booking_items.id','=','tb_booking_history.biId')
            ->leftJoin('tb_branch','tb_branch.id','=','tb_booking_items.biBranchId')
            ->select('tb_booking_history.*','tb_booking_items.biName as biName', 'tb_branch.branch_name as biBranchName')
            ->whereBetween('tb_booking_history.biDate', [$start_date, $end_date])
            ->orderBy('tb_booking_history.biDate', 'ASC')
            ->get();
       }else{
            $booking_history=DB::table('tb_booking_history')
            ->leftJoin('tb_booking_items','tb_booking_items.id','=','tb_booking_history.biId')
            ->leftJoin('tb_branch','tb_branch.id','=','tb_booking_items.biBranchId')
            ->select('tb_booking_history.*','tb_booking_items.biName as biName', 'tb_branch.branch_name as biBranchName')
            ->whereBetween('tb_booking_history.biDate', [$start_date, $end_date])
            ->where('tb_booking_history.biId','=',$request->biId)
            ->orderBy('tb_booking_history.biDate', 'ASC')
            ->get();
       }
        return view('backend.booking.date_wise_booking_report_data',compact('booking_history','request'));
    }


}
