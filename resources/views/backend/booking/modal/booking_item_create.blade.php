<div id="createBookingItem" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content animated bounceInLeft">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add New Booking Item</h4>
            </div>
            <div class="modal-body">
                    <!-- Error list Start -->
                    <span id="form_result"></span>
                    @if ($errors->any())
                    <div id="alert_message" class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <!-- Error list End -->
                <div class="panel panel-default">
                    <form id="modal_form">
                        @csrf
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="biName">Item Name</label>
                            <input type="text" class="form-control" id="biName" name="biName" placeholder="Item Name" required autocomplete="off">
                        </div>
                        
                        <div class="form-group">
                            <label for="biDescription">Item Description</label>
                            <textarea id="biDescription" name="biDescription" cols="5" rows="5" class="form-control" placeholder="Item Description" autocomplete="off"></textarea>
                        </div>
                        <hr>
                        
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-info pull-right" id="booking_item_add"><i class="fa fa-save"></i> Save</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

