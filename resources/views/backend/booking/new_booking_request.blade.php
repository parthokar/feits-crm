@extends('layout.master')
@section('title', 'New Booking Request')
@section('content')
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title"> New Booking Request</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{url('/')}}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li><a href="#"> New Booking Request </a>&nbsp;&nbsp;</li>
        </ol>
        <div class="clearfix"></div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->

    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-blue">
                    <div class="panel-heading">
                        
                    </div>
                    <div class="panel-body table-responsive">
                    <span id="form_result"></span>
                    <form id="booking_request_form">
                        @csrf
                        <div class="form-group">
                            <label for="branch_id" class="pull-left">
                                <h5><b>Select Branch</b><span class='require'>*</span></h5>
                            </label>
                            <div>
                                <select id="branch_id" name="branch_id" class="form-control" required="">
                                    <option value="">Select Branch</option>
                                </select>
                                <b class="form-text text-danger pull-left" id="studentError"></b>
                            </div>
                        </div>
                        <div class="form-group">
                            <label ><b>Booking Item <span class='require'>*</span></b></label>
                            <select name="biId" class="form-control" id="biId" required="">
                                <option selected="" value="">Select Booking Item</option>
                            </select>
                        </div>

                        <div class="form-group">
                          <label for="bookedBy"> <b>Booked By  <span class='require'>*</span></b></label>
                          <input type="text" id="bookedBy" name="bookedBy" class="form-control" required="" placeholder="Booked By" >
                        </div>
                        <div class="form-group">
                          <label for="purpose"> <b>Purpose  <span class='require'>*</span></b></label>
                          <textarea id="purpose" name="purpose" class="form-control" required="" placeholder="Purpose of booking" rows="6" ></textarea>
                        </div>
                        <div class="form-group">
                          <label for="biDate"> <b>Booking Date <span class='require'>*</span></b></label>
                              <input type='text' class="form-control" readonly="" required="" id="date_picker" name="biDate" placeholder="Click here to select date."  />
                        </div> 
                        <br>
                        <div class="form-group">
                          <label for="vbStartTime"> <b>Booking Starting Time  <span class='require'>*</span></b></label>
                          <input type="text" id="biStartTime" name="biStartTime" required="" class="form-control" readonly="" placeholder="Click here to select booking starting time." >
                        </div>
                        <div class="form-group">
                          <label for="vbEndTime"> <b>Booking Ending Time <span class='require'>*</span></b></label>
                          <input type="text" id="biEndTime" name="biEndTime" required="" class="form-control" readonly="" placeholder="Click here to select booking ending time." >
                        </div>
                        <div class="form-group">
                            <hr>
                        </div>
                      </form>
                          <br>
                          <button id="submit_booking_request" class="btn btn-primary">Submit Booking Request</button>
                          <br>
                          <br>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('extra_js')
<script>

      $("#biId").select2({
          placeholder: "Select Booking Item"
      });

      $('#date_picker').datetimepicker({
          pickTime: false,
          startDate: new Date()
      });

      $('#biStartTime').datetimepicker({
          pickDate: false
      });

      $('#biEndTime').datetimepicker({
          pickDate: false
      });


      $(document).ready(function(){
        $.ajax({
            url:"{{route('ajax.get_branch')}}",
            method:"get",
            success:function (response) {
                console.log(response);
                $('#branch_id').html(response);
            }
        });
       
        //get booking item
        $('#branch_id').on('change',function () {
            var id = $("#branch_id").val();
            //alert(id);
            $.ajax({
                type: "GET",
                url:"{{url('/ajax/get_booking_item_by_branch_id')}}"+"/"+id,
                success:function (response) {
                    //console.log(response);
                    $('#biId').html(response);
                    $("#biId").select2({
                        placeholder: "Select Booking Item"
                    });
                },
                error:function(xhr){

                }
            });
        });

          //add new booking item
        $( "#submit_booking_request" ).click(function() {

          var _token = '{{ csrf_token() }}';
          var biBranchId = $("#branch_id").val();
          var biId = $("#biId").val();
          var bookedBy = $("#bookedBy").val();
          var purpose = $("#purpose").val();
          var biDate = $("#date_picker").val();
          var biStartTime = $("#biStartTime").val();
          var biEndTime = $("#biEndTime").val();

              $.ajax({
                  url:"{{route('booking.save_new_request')}}",
                  method:"POST",
                  data: {_token : _token, biId : biId, bookedBy : bookedBy, purpose: purpose, biDate: biDate, biStartTime: biStartTime,  biEndTime: biEndTime, biBranchId: biBranchId },
                  success:function (response) {
                      var html = '';
                      if(response.errors)
                      {
                          html = '<div class="alert alert-danger">';

                            html += '<p><b>Error found: </b><br></p>';
                          for(var count = 0; count < response.errors.length; count++)
                          {
                              html += '<p>' + response.errors[count] + '</p>';
                          }
                          html += '</div>';
                          $('#form_result').html(html);
                      }else
                      {
                        swal({
                          title: response.title,
                          text: response.message,
                          icon: response.icon
                        });

                        $("#biId").val('').trigger('change');
                        $('#booking_request_form')[0].reset();
                      }
                  }
              });
          });
      });
  </script>
@endsection