@extends('layout.master')
@section('title', 'Booking Report')
@section('content')
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Booking Report</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{url('/')}}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li><a href="#">Booking Report</a>&nbsp;&nbsp;</li>
        </ol>
        <div class="clearfix"></div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->

    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-blue">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                Booking Report
                            </div>
                            <div class="col-md-6" style="text-align: right;">
                            </div>
                        </div>
                    </div>
                    <div class="panel-body table-responsive">
                        
                        {!! Form::open(['method'=>'post','route'=>'booking.report.booking_report_data']) !!}

                          <div class="col-md-6 ex-form">
                              <div class="form-group">
                                  <div class="input-form-gap"></div>
                                  <label class="col-md-4"><b>Select Branch</b><span class="clon">:</span> <span class='require'>*</span></label>
                                  <div class="col-md-8">
                                      <select id="branch_id" name="branch_id" class="form-control" required="">
                                        <option value="">Select Branch</option>
                                      </select>
                                  </div>
                              </div>
                              <br />
                              <br />
                              <div class="form-group">
                                <label class="col-md-4">Booking Item : </label>
                                  <div class="col-md-8">
                                    <select name="biId" class="form-control" id="biId" >
                                    </select>
                                  </div>
                              </div>
                              <br />
                              <div class="form-group">
                                  <div class="input-form-gap"></div>
                                  <label class="col-md-4">Start Date <span class="clon">:</span> <span class='require'>*</span></label>
                                  <div class="col-md-8">
                                      <input type="text" id="start_date" name="start_date" class="form-control" autocomplete="off" required="" placeholder="Select a date...">
                                  </div>
                              </div>
                              <br />
                              <div class="form-group">
                                  <div class="input-form-gap"></div>
                                  <label class="col-md-4">End Date <span class="clon">:</span> <span class='require'>*</span></label>
                                  <div class="col-md-8">
                                      <input type="text" id="end_date" name="end_date" class="form-control" autocomplete="off" required="" placeholder="Select a date...">
                                  </div>
                              </div>


                              <div class="form-group">
                                  <div class="input-form-gap"></div>
                                  <br>
                                  <div class="col-md-4">
                                  </div>
                                  <div class="col-md-8">
                                    <br>
                                      <button name="preview" value="preview" type="submit" class="btn btn-success">
                                      <i class="fa fa-search"></i> Preview</button>
                                  </div>

                              <br>
                              <br>
                              </div>
                          </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra_js')
  <script>

      $(document).ready(function(){
        $.ajax({
            url:"{{route('ajax.get_branch')}}",
            method:"get",
            success:function (response) {
                console.log(response);
                $('#branch_id').html(response);
            }
        });

        //get booking item
        $('#branch_id').on('change',function () {
            var id = $("#branch_id").val();
            //alert(id);
            $.ajax({
                type: "GET",
                url:"{{url('/ajax/get_booking_item_by_branch_id')}}"+"/"+id,
                success:function (response) {
                    //console.log(response);
                    $('#biId').html(response);
                    $("#biId").select2({
                        placeholder: "Select Booking Item"
                    });
                },
                error:function(xhr){

                }
            });
        });

      });
      
      $("#branch_id").select2();

      $('#start_date').datetimepicker({
          pickTime: false
      });

      $('#end_date').datetimepicker({
          pickTime: false
      });
  </script>
@endsection