@extends('layout.master')
@section('title', 'Booking Item List')
@section('content')
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Booking Items</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{url('/')}}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li><a href="#">Booking Item </a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active"> List</li>
        </ol>
        <div class="clearfix"></div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->

    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-blue">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                Booking Item List
                            </div>
                            <div class="col-md-6" style="text-align: right;">
                                <a href="" class="add-new-modal btn btn-success btn-round btn-sm" data-toggle="modal" data-target="#createBookingItem"> <i class="fa fa-plus"></i> Add New</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body table-responsive">
                        <table id="bookingItems" class="table table-striped table-bordered" >
                            <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Branch Name</th>
                                    <th>Created By</th>
                                    <th>Status</th>
                                    <th width="10%">#</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    <!-- Modal Start -->
    @include('backend.booking.modal.booking_item_create')
    @include('backend.booking.modal.booking_item_edit')
    <!-- Modal End -->
    </div>
@endsection

@section('extra_js')
<script>
    $(document).ready(function(){
    
        $('#bookingItems').DataTable({
            processing: true,
            serverSide: true,
            "order": [[ 1, "asc" ]],
            ajax:{
                url: "{{ route('booking.bookingitems') }}",
            },
            columns:[
                { 
                    data: 'DT_RowIndex', 
                    name: 'DT_RowIndex' 
                },

                {
                    data: 'biName',
                    name: 'biName'
                },

                {
                    data: 'biDescription',
                    name: 'biDescription'
                },

                {
                    data: 'biBranchName',
                    name: 'biBranchName'
                },
                
                {
                    data: 'created_by',
                    name: 'created_by'
                },

                {
                    data: 'status',
                    name: 'status',
                    render: function(data){
                        return data == '1' ? '<span style="color:green">Active</span>' : '<span style="color:red">InActive</span>'
                    },
                },

                {
                    data: 'action',
                    name: 'action',
                    orderable: false
                }
            ]
        });
    });

    function confirmDelete(id) {
      swal({
          title: "Are you sure to delete?",
          text: "You will not be able to recover this data !",
          icon: "warning",
          buttons: [
            'No, cancel it!',
            'Yes, I am sure!'
          ],
          dangerMode: true,
        }).then(function(isConfirm) {
          if (isConfirm) {
            $.ajax({
                 type: "GET",
                 url:"{{url('/booking/items/delete')}}"+"/"+id,
                 dataType:"json",
                 success:function(response){
                    $('#bookingItems').DataTable().ajax.reload();

                    swal({
                      title: response.title,
                      text: response.message,
                      icon: response.icon
                    });
                 }
                })

          } else {
            swal("Cancelled", " Operation has been Cancelled.", "error");
          }
        })
    }

    function confirmInactive(id) {
      swal({
          title: "Are you sure to Deactivate?",
          text: "You will be able to recover this data !",
          icon: "warning",
          buttons: [
            'No, cancel it!',
            'Yes, I am sure!'
          ],
          dangerMode: true,
        }).then(function(isConfirm) {
          if (isConfirm) {
            $.ajax({
                 type: "GET",
                 url:"{{url('booking/item/inactive')}}"+"/"+id,
                 dataType:"json",
                 success:function(response){
                    $('#bookingItems').DataTable().ajax.reload();

                    swal({
                      title: response.title,
                      text: response.message,
                      icon: response.icon
                    });
                 }
                })

          } else {
            swal("Cancelled", " Operation has been Cancelled.", "error");
          }
        })
    }


    function confirmActive(id) {
      swal({
          title: "Are you sure to activate?",
          text: "You will be able to recover this data!",
          icon: "success",
          buttons: [
            'No, cancel it!',
            'Yes, I am sure!'
          ],
          dangerMode: false,
        }).then(function(isConfirm) {
          if (isConfirm) {
            $.ajax({
                 type: "GET",
                 url:"{{url('booking/item/active')}}"+"/"+id,
                 dataType:"json",
                 success:function(response){
                    $('#bookingItems').DataTable().ajax.reload();

                    swal({
                      title: response.title,
                      text: response.message,
                      icon: response.icon
                    });
                 }
                })

          } else {
            swal("Cancelled", " Operation has been Cancelled.", "error");
          }
        })
    }



    //add new booking item
    $( "#booking_item_add" ).click(function() {
    var _token = '{{ csrf_token() }}';
    var biName = $("#biName").val();
    var biDescription = $("#biDescription").val();
        // console.log(biName);
        // console.log(biDescription);
        $.ajax({
            url:"{{route('booking_item.store')}}",
            method:"POST",
            data: {_token : _token, biName : biName, biDescription : biDescription},
            success:function (response) {
                var html = '';
                if(response.errors)
                {
                    html = '<div class="alert alert-danger">';
                    for(var count = 0; count < response.errors.length; count++)
                    {
                        html += '<p>' + response.errors[count] + '</p>';
                    }
                    html += '</div>';
                    $('#form_result').html(html);
                }
                if(response.success)
                {
                    swal(response.success, "", "success");
                    $('#modal_form')[0].reset();
                    $('#bookingItems').DataTable().ajax.reload();
                    $('#createBookingItem').modal('hide');
                }
                
            }
        });
    });

        //Edit booking item information 
        $(document).on('click', '.edit', function(){
            var id = $(this).attr('id');
            //alert(id);
            //$('#form_result').html('');
            $.ajax({
             type: "GET",
             url:"{{url('/booking/item/edit')}}"+"/"+id,
             dataType:"json",
             success:function(response){
                 console.log(response);
                 $('#id').val(response.id);
                 $('#edit_biName').val(response.biName);
                 $('#edit_biDescription').val(response.biDescription);
                 $("#status option[value=" + response.status + "]").prop('selected', true);
             }
            })
           });

        
        //update booking item information 
        $( "#booking_item_update" ).click(function() {
            var _token = '{{ csrf_token() }}';
            var id = $("#id").val();
            var edit_biName = $("#edit_biName").val();
            var edit_biDescription = $("#edit_biDescription").val();
            var status = $("#status").val();

                $.ajax({
                    url:"{{route('booking_item.update')}}",
                    method:"post",
                    data: {_token : _token, id : id, biName : edit_biName, biDescription : edit_biDescription, status : status},
                    success:function (response) {
                        console.log(response);
                        var html = '';
                        if(response.errors)
                        {
                            html = '<div class="alert alert-danger">';
                            
                            html += '<p>' + response.errors + '</p>';
                            
                            html += '</div>';
                            $('#edit_form_result').html(html);
                        }
                        if(response.falied)
                        {
                            swal(response.falied, "", "warning");
                        }
                        if(response.success)
                        {
                            swal(response.success, "", "success");
                            $('#edit_form_result').hide();
                            $('#modal_form')[0].reset();
                            $('#bookingItems').DataTable().ajax.reload();
                            $('#editBookingItem').modal('hide');
                        }
                        
                    }
                });
                
            });
    </script>

@endsection