@extends('layout.master')
@section('title', 'Booking Report')
@section('content')
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Booking Report</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{url('/')}}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li><a href="#">Booking Report</a>&nbsp;&nbsp;</li>
        </ol>
        <div class="clearfix"></div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->

    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-blue">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                Booking Report
                            </div>
                            <div class="col-md-6" style="text-align: right;">
                            </div>
                        </div>
                    </div>
                    <div class="panel-body table-responsive">
                        
                        <table id="table" class="table table-hover table-bordered table-dynamic">
                            <thead>
                            <tr>
                                <th>SN</th>
                                <th>Item Name</th>
                                <th>Branch Name</th>
                                <th>Booked By</th>
                                <th width="20%">Purpose</th>
                                <th>Date</th>
                                <th>Starting Time</th>
                                <th>Ending Time</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $i=0; @endphp
                            @foreach($booking_history as $bh)
                                <tr>
                                    <td>{{++$i}}</td>
                                    <td>{{$bh->biName}}</td>
                                    <td>{{$bh->biBranchName}}</td>
                                    <td>{{$bh->bookedBy}}</td>
                                    <td>{{$bh->purpose}}</td>
                                    <td>{{\Carbon\Carbon::parse($bh->biDate)->format('d-M-Y')}}</td>
                                    <td>{{date('h:i:s a',strtotime($bh->biStartTime))}}</td>
                                    <td>{{date('h:i:s a',strtotime($bh->biEndTime))}}</td>
                                    <td><?php if($bh->status==1){ echo "<span style='color:green;'>Active</span>"; }
                                    if($bh->status==0){ echo "<span style='color:red;'>Cancelled</span>"; } ?></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra_js')
  <script type="text/javascript">
     $('#table').DataTable();
  </script>
@endsection