@extends('layout.master')
@section('title', 'Booking List')
@section('content')
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Booking</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{url('/')}}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li><a href="#">Booking </a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active"> List</li>
        </ol>
        <div class="clearfix"></div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->

    <!--Flash Message Start-->
    @if(Session::has('success'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('success') }}</p>
    @endif
    @if(Session::has('error'))
        <p id="alert_message" class="alert alert-error">{{ Session::get('error') }}</p>
    @endif
    @if(Session::has('delete'))
        <p id="alert_message" class="alert alert-danger">{{ Session::get('delete') }}</p>
    @endif
    <!--Flash Message End-->
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-blue">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                Booking List
                            </div>
                            <div class="col-md-6" style="text-align: right;">
                            </div>
                        </div>
                    </div>
                    <div class="panel-body table-responsive">
                        <table id="booking" class="table table-striped table-bordered" >
                            <thead>
                            <tr>
                                <th>SN</th>
                                <th>Item Name</th>
                                <th>Branch Name</th>
                                <th>Booked By</th>
                                <th width="15%">Purpose</th>
                                <th>Date</th>
                                <th>Starting Time</th>
                                <th>Ending Time</th>
                                <th>Status</th>
                                <th>Created By</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra_js')
<script>
    $(document).ready(function(){
    
        $('#booking').DataTable({
            processing: true,
            serverSide: true,
            "order": [[ 5, "asc" ]],
            ajax:{
                url: "{{ route('booking.list') }}",
            },
            columns:[
                { 
                    data: 'DT_RowIndex', 
                    name: 'DT_RowIndex' 
                },
                {
                    data: 'biName',
                    name: 'biName'
                },
                {
                    data: 'biBranchName',
                    name: 'biBranchName'
                },
                {
                    data: 'bookedBy',
                    name: 'bookedBy'
                },
                {
                    data: 'purpose',
                    name: 'purpose'
                },
                {
                    data: 'biBDate',
                    name: 'biBDate'
                },
                {
                    data: 'biStartTime',
                    name: 'biStartTime'
                },
                {
                    data: 'biEndTime',
                    name: 'biEndTime'
                },
                {
                    data: 'status',
                    name: 'status',
                    render: function(data){
                        return data == '1' ? '<span style="color:green">Active</span>' : '<span style="color:red">InActive</span>'
                    },
                },
                
                {
                    data: 'created_by',
                    name: 'created_by'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false
                }
            ]
        });
    });

    function confirmDelete(id) {
      swal({
          title: "Are you sure to delete?",
          text: "You will not be able to recover this data !",
          icon: "warning",
          buttons: [
            'No, cancel it!',
            'Yes, I am sure!'
          ],
          dangerMode: true,
        }).then(function(isConfirm) {
          if (isConfirm) {
            $.ajax({
                 type: "GET",
                 url:"{{url('booking/booking_delete')}}"+"/"+id,
                 dataType:"json",
                 success:function(response){
                    $('#booking').DataTable().ajax.reload();

                    swal({
                      title: response.title,
                      text: response.message,
                      icon: response.icon
                    });
                 }
                })

          } else {
            swal("Cancelled", " Operation has been Cancelled.", "error");
          }
        })
    }

    function confirmCancel(id) {
      swal({
          title: "Are you sure to cancel request?",
          text: "You will be able to recover this data!",
          icon: "warning",
          buttons: [
            'No, cancel it!',
            'Yes, I am sure!'
          ],
          dangerMode: true,
        }).then(function(isConfirm) {
          if (isConfirm) {
            $.ajax({
                 type: "GET",
                 url:"{{url('booking/booking_cancel')}}"+"/"+id,
                 dataType:"json",
                 success:function(response){
                    $('#booking').DataTable().ajax.reload();

                    swal({
                      title: response.title,
                      text: response.message,
                      icon: response.icon
                    });
                 }
                })

          } else {
            swal("Cancelled", " Operation has been Cancelled.", "error");
          }
        })
    }

    function confirmActive(id) {
      swal({
          title: "Are you sure to active request?",
          text: "You will be able to recover this data!",
          icon: "success",
          buttons: [
            'No, cancel it!',
            'Yes, I am sure!'
          ],
          dangerMode: false,
        }).then(function(isConfirm) {
          if (isConfirm) {
            $.ajax({
                 type: "GET",
                 url:"{{url('booking/booking_request_active')}}"+"/"+id,
                 dataType:"json",
                 success:function(response){
                    $('#booking').DataTable().ajax.reload();

                    swal({
                      title: response.title,
                      text: response.message,
                      icon: response.icon
                    });
                 }
                })

          } else {
            swal("Cancelled", " Operation has been Cancelled.", "error");
          }
        })
    }
    </script>

@endsection