@extends('layout.master')
@section('title', 'Vehicle Booking Report')
@section('content')
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Vehicle Booking Report</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{url('/')}}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li><a href="#">Vehicle Booking Report</a>&nbsp;&nbsp;</li>
        </ol>
        <div class="clearfix"></div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->

    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-blue">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                <p><i class="fa fa-car"></i> <strong>Booked Vehicle </strong> List from <b>{{\Carbon\Carbon::parse($request->start_date)->format('d-M-Y')}}</b> to <b>{{\Carbon\Carbon::parse($request->end_date)->format('d-M-Y')}}</b></p>
                            </div>
                            <div class="col-md-6" style="text-align: right;">
                            </div>
                        </div>
                    </div>
                    <div class="panel-body table-responsive">
                        
                       <table id="table" class="table table-hover table-bordered table-dynamic">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Vehicle Name</th>
                                    <th>Registration Number</th>
                                    <th>Branch Name</th>
                                    <th>Booked By</th>
                                    <th>Driver Name</th>
                                    <th>Booked Date</th>
                                    <th>Booked Time</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i=0; @endphp
                                @foreach($booking_list as $vi)
                                <tr>
                                    <td>{{++$i}}</td>
                                    <td>{{$vi->vehicle_name}}</td>
                                    <td>{{$vi->registrationNumber}}</td>
                                    <td>{{$vi->branchName}}</td>
                                    <td>{{$vi->booked_by}}</td>
                                    <td>{{$vi->driver_name}}</td>
                                    <td>{{date('d-m-Y', strtotime($vi->vb_date))}}</td>
                                    <td>{{date("h:i a", strtotime($vi->vb_startTime))}}-{{date("h:i a", strtotime($vi->vb_EndTime))}}</td>
                                    <td><?php if($vi->status==1){ echo "<span style='color:green;'>Active</span>"; }
                                    if($vi->status==0){ echo "<span style='color:red;'>Cancelled</span>"; } ?></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra_js')
  <script type="text/javascript">
     $('#table').DataTable();
  </script>
@endsection