<div id="createNewVehicle" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content animated bounceInLeft">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add New Vehicle Information</h4>
            </div>
            <div class="modal-body">
                    <!-- Error list Start -->
                    <span id="form_result"></span>
                    @if ($errors->any())
                    <div id="alert_message" class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <!-- Error list End -->
                <div class="panel panel-default">
                    <form id="modal_form">
                        @csrf
                    <div class="panel-body">
                        <span id="form_result"></span>
                        <div class="form-group">
                            <label for="branch_id" class="pull-left">
                                <b>Select Branch</b><span class='require'>*</span>
                            </label>
                            <div>
                                <select id="branch_id" name="branch_id" class="form-control">
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="vehicle_name"><b>Vehicle Name</b><span class='require'>*</span></label>
                            <input type="text" class="form-control" id="vehicle_name" name="vehicle_name" placeholder="Vehicle Name" autocomplete="off">
                        </div>
                        
                        <div class="form-group">
                            <label for="engine_machine_number"><b>Engine/Machine Number</b></label>
                            <input type="text" class="form-control" id="engine_machine_number" name="engine_machine_number" placeholder="Engine/Machine Number" autocomplete="off">
                        </div>
                        
                        <div class="form-group">
                            <label for="registration_number"><b>Registration Number</b></label>
                            <input type="text" class="form-control" id="registration_number" name="registration_number" placeholder="Registration Number" autocomplete="off">
                        </div>
                        
                        
                        <div class="form-group">
                            <label for="insurance_number"><b>Insurance Number</b></label>
                            <input type="text" class="form-control" id="insurance_number" name="insurance_number" placeholder="Insurance Number" autocomplete="off">
                        </div>
                        
                        <div class="form-group">
                            <label for="remarks"><b>Remarks</b></label>
                            <textarea id="remarks" name="remarks" cols="5" rows="5" class="form-control" placeholder="Remarks" autocomplete="off"></textarea>
                        </div>
                        <hr>
                        
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-info pull-right" id="new_vehicle_add"><i class="fa fa-save"></i> Save Information</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

