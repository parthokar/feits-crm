@extends('layout.master')
@section('title', 'Expense Category List')
@section('content')
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Expense Category List</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{url('/')}}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li><a href="#">Expense Category List </a>&nbsp;&nbsp;</li>
        </ol>
        <div class="clearfix"></div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->

    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-blue">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                Add New Expense Category
                            </div>
                            <div class="col-md-6" style="text-align: right;">
                            </div>
                        </div>
                    </div>
                    <div class="panel-body table-responsive">
                      <form id="modal_form">
                        @csrf
                        <div class="form-group">
                          <label for="vcategory_name"> Category Name</label>
                          <input type="text" id="vcategory_name" name="vcategory_name" class="form-control" required="" placeholder="Category Name" >
                        </div>
                        <div class="form-group">
                          <label for="vcat_description"> Category Description</label>
                          <textarea name="vcat_description" id="vcat_description" rows="5" class="form-control" placeholder="Category Description" ></textarea>
                        </div>
                        <hr>
                        <button type="button" class="btn btn-success" id="add_new_vehicle_expense"><i class="fa fa-save"></i> Save Category</button>
                      </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-blue">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                Expense Category
                            </div>
                            <div class="col-md-6" style="text-align: right;">
                            </div>
                        </div>
                    </div>
                    <div class="panel-body table-responsive">
                        <table id="vehicleExpenseCategoryLists" class="table table-striped table-bordered" >
                            <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Category Name</th>
                                    <th>Description</th>
                                    <th>Created By</th>
                                    <th>Status</th>
                                    <th width="10%">#</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    <!-- Modal Start -->
    @include('backend.vehicle_booking.modal.expenses.expenses_edit')
    <!-- Modal End -->
    </div>
@endsection

@section('extra_js')
<script>
    $(document).ready(function(){
    
        $('#vehicleExpenseCategoryLists').DataTable({
            processing: true,
            serverSide: true,
            "order": [[ 1, "asc" ]],
            ajax:{
                url: "{{ route('vehicle_expense.expense_category_list') }}",
            },
            columns:[
                { 
                    data: 'DT_RowIndex', 
                    name: 'DT_RowIndex' 
                },

                {
                    data: 'vcategory_name',
                    name: 'vcategory_name'
                },

                {
                    data: 'vcat_description',
                    name: 'vcat_description'
                },            
                
                {
                    data: 'createdBy',
                    name: 'createdBy'
                },

                {
                    data: 'status',
                    name: 'status',
                    render: function(data){
                        return data == '1' ? '<span style="color:green">Active</span>' : '<span style="color:red">Inactive</span>'
                    },
                },

                {
                    data: 'action',
                    name: 'action',
                    orderable: false
                }
            ]
        });

      $.ajax({
          url:"{{route('ajax.get_branch')}}",
          method:"get",
          success:function (response) {
              console.log(response);
              $('#branch_id').html(response);
          }
      });
     
      $("#branch_id").select2();

    });


    function confirmDelete(id) {
      swal({
          title: "Are you sure to delete?",
          text: "You will not be able to recover this data !",
          icon: "warning",
          buttons: [
            'No, cancel it!',
            'Yes, I am sure!'
          ],
          dangerMode: true,
        }).then(function(isConfirm) {
          if (isConfirm) {
            $.ajax({
                 type: "GET",
                 url:"{{url('/vehicle/expense_category/delete')}}"+"/"+id,
                 dataType:"json",
                 success:function(response){
                    $('#vehicleExpenseCategoryLists').DataTable().ajax.reload();

                    swal({
                      title: response.title,
                      text: response.message,
                      icon: response.icon
                    });
                 }
                })

          } else {
            swal("Cancelled", " Operation has been Cancelled.", "error");
          }
        })
    }

    function confirmInactive(id) {
      swal({
          title: "Are you sure to Inactive?",
          text: "You will be able to recover this data !",
          icon: "warning",
          buttons: [
            'No, cancel it!',
            'Yes, I am sure!'
          ],
          dangerMode: true,
        }).then(function(isConfirm) {
          if (isConfirm) {
            $.ajax({
                 type: "GET",
                 url:"{{url('/vehicle/expense_category/inactive')}}"+"/"+id,
                 dataType:"json",
                 success:function(response){
                    $('#vehicleExpenseCategoryLists').DataTable().ajax.reload();

                    swal({
                      title: response.title,
                      text: response.message,
                      icon: response.icon
                    });
                 }
                })

          } else {
            swal("Cancelled", " Operation has been Cancelled.", "error");
          }
        })
    }


    function confirmActive(id) {
      swal({
          title: "Are you sure to active?",
          text: "You will be able to recover this data!",
          icon: "warning",
          buttons: [
            'No, cancel it!',
            'Yes, I am sure!'
          ],
          dangerMode: false,
        }).then(function(isConfirm) {
          if (isConfirm) {
            $.ajax({
                 type: "GET",
                 url:"{{url('/vehicle/expense_category/active')}}"+"/"+id,
                 dataType:"json",
                 success:function(response){
                    $('#vehicleExpenseCategoryLists').DataTable().ajax.reload();

                    swal({
                      title: response.title,
                      text: response.message,
                      icon: response.icon
                    });
                 }
                })

          } else {
            swal("Cancelled", " Operation has been Cancelled.", "error");
          }
        })
    }



    //add new vehicle expense information
    $( "#add_new_vehicle_expense" ).click(function() {
    var _token = '{{ csrf_token() }}';
    var vcategory_name = $("#vcategory_name").val();
    var vcat_description = $("#vcat_description").val();

        $.ajax({
            url:"{{route('vehicle_expense.expense_category_store')}}",
            method:"POST",
            data: {_token : _token, vcategory_name : vcategory_name, vcat_description : vcat_description },
            success:function (response) {
                var html = '';
                if(response.errors)
                {
                    html = '<div class="alert alert-danger">';
                    for(var count = 0; count < response.errors.length; count++)
                    {
                        html += '<p>' + response.errors[count] + '</p>';
                    }
                    html += '</div>';
                    $('#form_result').html(html);
                }else
                {
                    swal({
                      title: response.title,
                      text: response.message,
                      icon: response.icon
                    });

                    if(response.icon=='success'){
                      $('#modal_form')[0].reset();
                      $('#vehicleExpenseCategoryLists').DataTable().ajax.reload();
                    }
                }
                
            }
        });
    });

      //Edit booking item information 
      $(document).on('click', '.edit', function(){
          var id = $(this).attr('id');
          //alert(id);
          //$('#form_result').html('');
          $.ajax({
           type: "GET",
           url:"{{url('/booking/item/edit')}}"+"/"+id,
           dataType:"json",
           success:function(response){
               console.log(response);
               $('#id').val(response.id);
               $('#edit_biName').val(response.biName);
               $('#edit_biDescription').val(response.biDescription);
               $("#status option[value=" + response.status + "]").prop('selected', true);
           }
          })
         });

      
      //update booking item information 
      $( "#booking_item_update" ).click(function() {
          var _token = '{{ csrf_token() }}';
          var id = $("#id").val();
          var edit_biName = $("#edit_biName").val();
          var edit_biDescription = $("#edit_biDescription").val();
          var status = $("#status").val();

              $.ajax({
                  url:"{{route('booking_item.update')}}",
                  method:"post",
                  data: {_token : _token, id : id, biName : edit_biName, biDescription : edit_biDescription, status : status},
                  success:function (response) {
                      console.log(response);
                      var html = '';
                      if(response.errors)
                      {
                          html = '<div class="alert alert-danger">';
                          
                          html += '<p>' + response.errors + '</p>';
                          
                          html += '</div>';
                          $('#edit_form_result').html(html);
                      }
                      if(response.falied)
                      {
                          swal(response.falied, "", "warning");
                      }
                      if(response.success)
                      {
                          swal(response.success, "", "success");
                          $('#edit_form_result').hide();
                          $('#modal_form')[0].reset();
                          $('#bookingItems').DataTable().ajax.reload();
                          $('#editBookingItem').modal('hide');
                      }
                      
                  }
              });
              
          });

    </script>

@endsection