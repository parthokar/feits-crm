@extends('layout.master')
@section('title', 'New Booking Request')
@section('content')
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title"> New Booking Request</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{url('/')}}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li><a href="#"> New Booking Request </a>&nbsp;&nbsp;</li>
        </ol>
        <div class="clearfix"></div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->

    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-blue">
                    <div class="panel-heading">
                        
                    </div>
                    <div class="panel-body table-responsive">
                    <span id="form_result"></span>
                    <form id="booking_request_form">
                        @csrf
                        <div class="form-group">
                            <label for="branch_id" class="pull-left">
                                <h5><b>Select Branch </b><span class='require'>*</span></h5>
                            </label>
                            <div>
                                <select id="branch_id" name="branch_id" class="form-control" required="">
                                    <option value="">Select Branch</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label><b>Booking Vehicle <span class='require'>*</span></b></label>
                            <select name="vehicle_id" class="form-control" id="vehicle_id" required="">
                            </select>
                        </div>

                        
                        <div class="form-group">
                          <label for="booked_by"><b>Booked By</b> <span class='require'>*</span></label>
                          <input type="text" id="booked_by" name="booked_by" class="form-control" required="" placeholder="Booked By" >
                        </div>

                        <div class="form-group">
                          <label for="driver_name"><b>Driver Name</b> <span class='require'>*</span></label>
                          <input type="text" id="driver_name" name="driver_name" class="form-control" required="" placeholder="Driver Name" >
                        </div>
                        <div class="form-group">
                          <label for="vb_date"><b>Booking Date <span class='require'>*</span></b></label>
                              <input type='text' class="form-control" readonly="" required="" id="date_picker" name="vb_date" placeholder="Click here to select date."  />
                        </div> 
                        <br>
                        <div class="form-group">
                          <label for="vb_startTime"><b>Booking Starting Time  <span class='require'>*</span></b></label>
                          <input type="text" id="vb_startTime" name="vb_startTime" required="" class="form-control" readonly="" placeholder="Click here to select booking starting time." >
                        </div>
                        <div class="form-group">
                          <label for="vb_EndTime"><b>Booking Ending Time <span class='require'>*</span></b></label>
                          <input type="text" id="vb_EndTime" name="vb_EndTime" required="" class="form-control" readonly="" placeholder="Click here to select booking ending time." >
                        </div>
                        <div class="form-group">
                          <label for="purpose"><b>Purpose</b></label>
                          <textarea name="purpose" id="purpose" rows="5" class="form-control" placeholder="Booking Purpose" ></textarea>
                        </div>

                        <div class="form-group">
                            <hr>
                        </div>
                      </form>
                          <br>
                          <button id="submit_booking_request" class="btn btn-primary">Submit Booking Request</button>
                          <br>
                          <br>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('extra_js')
<script>

      $("#branch_id").select2();

      $('#date_picker').datetimepicker({
          pickTime: false,
          startDate: new Date()
      });

      $('#vb_startTime').datetimepicker({
          pickDate: false
      });

      $('#vb_EndTime').datetimepicker({
          pickDate: false
      });


      $(document).ready(function(){
        $.ajax({
            url:"{{route('ajax.get_branch')}}",
            method:"get",
            success:function (response) {
                console.log(response);
                $('#branch_id').html(response);
            }
        });
       
        //get booking item
        $('#branch_id').on('change',function () {
            var id = $("#branch_id").val();
            //alert(id);
            $.ajax({
                type: "GET",
                url:"{{url('/ajax/get_vehicle_list_by_branch_id')}}"+"/"+id,
                success:function (response) {
                    //console.log(response);
                    $('#vehicle_id').html(response);
                    $("#vehicle_id").select2({
                        placeholder: "Select a vehicle"
                    });
                },
                error:function(xhr){

                }
            });
        });

          //add new booking item
        $( "#submit_booking_request" ).click(function() {

          var _token = '{{ csrf_token() }}';
          var branch_id = $("#branch_id").val();
          var vehicle_id = $("#vehicle_id").val();
          var booked_by = $("#booked_by").val();
          var driver_name = $("#driver_name").val();
          var purpose = $("#purpose").val();
          var vb_date = $("#date_picker").val();
          var vb_startTime = $("#vb_startTime").val();
          var vb_EndTime = $("#vb_EndTime").val();

              $.ajax({
                  url:"{{route('vehicle.store_new_booking')}}",
                  method:"POST",
                  data: {_token : _token, vehicle_id : vehicle_id, booked_by : booked_by, driver_name: driver_name, purpose: purpose, vb_date: vb_date, vb_startTime: vb_startTime,  vb_EndTime: vb_EndTime, branch_id: branch_id },
                  success:function (response) {
                      var html = '';
                      if(response.errors)
                      {
                          html = '<div class="alert alert-danger">';

                            html += '<p><b>Error found: </b><br></p>';
                          for(var count = 0; count < response.errors.length; count++)
                          {
                              html += '<p>' + response.errors[count] + '</p>';
                          }
                          html += '</div>';
                          $('#form_result').html(html);
                      }else
                      {
                        swal({
                          title: response.title,
                          text: response.message,
                          icon: response.icon
                        });

                        $("#vehicle_id").val('').trigger('change');

                        if(response.icon=='success'){
                          $('#booking_request_form')[0].reset();
                        }
                      }
                  }
              });
          });
      });
  </script>
@endsection