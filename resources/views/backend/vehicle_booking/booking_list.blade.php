@extends('layout.master')
@section('title', 'Vehicle Booking List')
@section('content')
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">Vehicle Booking</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{url('/')}}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li><a href="#">Vehicle Booking </a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active"> List</li>
        </ol>
        <div class="clearfix"></div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->

    <!--Flash Message Start-->
    @if(Session::has('success'))
            <p id="alert_message" class="alert alert-success">{{ Session::get('success') }}</p>
    @endif
    @if(Session::has('error'))
        <p id="alert_message" class="alert alert-error">{{ Session::get('error') }}</p>
    @endif
    @if(Session::has('delete'))
        <p id="alert_message" class="alert alert-danger">{{ Session::get('delete') }}</p>
    @endif
    <!--Flash Message End-->
    <div class="page-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-blue">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                Vehicle Booking List
                            </div>
                            <div class="col-md-6" style="text-align: right;">
                            </div>
                        </div>
                    </div>
                    <div class="panel-body table-responsive">
                        <table id="vehicleBookingList" class="table table-striped table-bordered" >
                            <thead>
                            <tr>
                                <th>SN</th>
                                <th>Vehicle Name</th>
                                <th>Branch Name</th>
                                <th>Booked By</th>
                                <th>Driver Name</th>
                                <th width="15%">Purpose</th>
                                <th>Date</th>
                                <th>Starting Time</th>
                                <th>Ending Time</th>
                                <th>Status</th>
                                <th>Created By</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra_js')
<script>
    $(document).ready(function(){
    
        $('#vehicleBookingList').DataTable({
            processing: true,
            serverSide: true,
            "order": [[ 6, "asc" ]],
            ajax:{
                url: "{{ route('vehicle.booking_list') }}",
            },
            columns:[
                { 
                    data: 'DT_RowIndex', 
                    name: 'DT_RowIndex' 
                },
                {
                    data: 'vehicleName',
                    name: 'vehicleName'
                },
                {
                    data: 'branchName',
                    name: 'branchName'
                },
                {
                    data: 'booked_by',
                    name: 'booked_by'
                },
                {
                    data: 'driver_name',
                    name: 'driver_name'
                },
                {
                    data: 'purpose',
                    name: 'purpose'
                },
                {
                    data: 'vbDate',
                    name: 'vbDate'
                },
                {
                    data: 'vb_startTime',
                    name: 'vb_startTime'
                },
                {
                    data: 'vb_EndTime',
                    name: 'vb_EndTime'
                },
                {
                    data: 'status',
                    name: 'status',
                    render: function(data){
                        return data == '1' ? '<span style="color:green">Active</span>' : '<span style="color:red">Cancelled</span>'
                    },
                },
                
                {
                    data: 'createdBy',
                    name: 'createdBy'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false
                }
            ]
        });
    });

    function confirmDelete(id) {
      swal({
          title: "Are you sure to delete?",
          text: "You will not be able to recover this data !",
          icon: "warning",
          buttons: [
            'No, cancel it!',
            'Yes, I am sure!'
          ],
          dangerMode: true,
        }).then(function(isConfirm) {
          if (isConfirm) {
            $.ajax({
                 type: "GET",
                 url:"{{url('vehicle_booking/booking_delete')}}"+"/"+id,
                 dataType:"json",
                 success:function(response){
                    $('#vehicleBookingList').DataTable().ajax.reload();

                    swal({
                      title: response.title,
                      text: response.message,
                      icon: response.icon
                    });
                 }
                })

          } else {
            swal("Cancelled", " Operation has been Cancelled.", "error");
          }
        })
    }

    function confirmCancel(id) {
      swal({
          title: "Are you sure to cancel request?",
          text: "You will be able to recover this data!",
          icon: "warning",
          buttons: [
            'No, cancel it!',
            'Yes, I am sure!'
          ],
          dangerMode: true,
        }).then(function(isConfirm) {
          if (isConfirm) {
            $.ajax({
                 type: "GET",
                 url:"{{url('vehicle_booking/booking_cancel')}}"+"/"+id,
                 dataType:"json",
                 success:function(response){
                    $('#vehicleBookingList').DataTable().ajax.reload();

                    swal({
                      title: response.title,
                      text: response.message,
                      icon: response.icon
                    });
                 }
                })

          } else {
            swal("Cancelled", " Operation has been Cancelled.", "error");
          }
        })
    }

    function confirmActive(id) {
      swal({
          title: "Are you sure to activate request?",
          text: "You will be able to recover this data!",
          icon: "success",
          buttons: [
            'No, cancel it!',
            'Yes, I am sure!'
          ],
          dangerMode: false,
        }).then(function(isConfirm) {
          if (isConfirm) {
            $.ajax({
                 type: "GET",
                 url:"{{url('vehicle_booking/booking_request_active')}}"+"/"+id,
                 dataType:"json",
                 success:function(response){
                    $('#vehicleBookingList').DataTable().ajax.reload();

                    swal({
                      title: response.title,
                      text: response.message,
                      icon: response.icon
                    });
                 }
                })

          } else {
            swal("Cancelled", " Operation has been Cancelled.", "error");
          }
        })
    }
    </script>

@endsection