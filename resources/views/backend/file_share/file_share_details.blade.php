@extends('layout.master')
@section('title', 'File Details')
@section('extra_css')
<style>
    .team_profile{
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        transition: 0.3s;
        border-radius: 3px;
        cursor: pointer;
        margin-bottom: 20px;
    }
    .team_profile:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }
    .team_profile:hover .team_profile_img_overlay{
        opacity: 1;
        cursor: pointer;
    }
    .team_profile_img{
        height:200px;
        width: 100%;
        position: relative;
    }
    .team_profile_img_overlay{
        position: absolute;
        width: 100%;
        height: 100%;

        top: 0;
        left: 0;
        opacity:0;
        webkit-transition: all 0.3s ease-in-out;
        -moz-transition: all 0.3s ease-in-out;
        -ms-transition: all 0.3s ease-in-out;
        -o-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
        background: -webkit-linear-gradient(left, rgba(56,107,12,.6) 0%, rgba(133,153,115,.6) 100%);
        background: linear-gradient(to right, rgba(56,107,12,.6) 0%, rgba(133,153,115,.6) 100%);
        box-shadow: 0 5px 10px 0 rgba(56,107,12, 0.5)!important;
    }
    .team_profile_img_overlay i{
        color:#fff;
        position: relative;
        top:50%;
        left:50%;
        transform: translate(-50%,-50%);
        font-size: 20px;
    }

    .team_profile img {
        border-radius: 3px 3px 0 0;
        height: 100px;
        width: 100px;
    }

    .team_profile .profile_container {
        padding: 2px 16px;
        text-align: center;
        padding-top: 20px;
        padding-bottom: 20px;
    }
    .profile_container h4{
        margin-top: 5px;
        margin-bottom: 5px;
    }
    .profile_container p{
        margin-bottom: 5px;
        font-size: 13px;
    }
    .client_project_details .profile_container{
        text-align: left;
        padding-bottom: 10px;
    }
    .client_project_details{
        padding-bottom: 10px;
    }
    .client_project_btn{
        margin-left: 10px;
    }

</style>
@endsection


@section('content')
<!--BEGIN TITLE & BREADCRUMB PAGE-->
<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">File Details</div>
    </div>
    <ol class="breadcrumb page-breadcrumb pull-right">
        <li><i class="fa fa-home"></i>&nbsp;<a href="{{url('/')}}">Home</a>&nbsp;&nbsp;<i
                class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li><a href="#">File</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="active">File Details</li>
    </ol>
    <div class="clearfix"></div>
</div>
<!--END TITLE & BREADCRUMB PAGE-->
<div class="page-content">
        {{ Html::script('corporate/js/sweetalert.min.js') }}
        @if(Session::has('success'))
        <script>
            var msg =' <?php echo Session::get('success');?>'
            swal(msg, "", "success");
        </script>
        @endif
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-blue">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-6">
                            <i class="fa fa-tasks" style="font-size: 20px;"></i>
                            Shared With
                        </div>
                        <div class="col-md-6" style="text-align: right;">
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                 @if($data !== null && $data !== '')
                   @if(isset($data[0]->branch_name))
                  <h4 class="text-center">Branch: {{$data[0]->branch_name}}</h4>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-blue">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <i class="fa fa-tasks" style="font-size: 20px;"></i>
                                            Employee
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                  @foreach($employee  as $employees)
                                  <a target="_blank" href="{{url('employee/profile')."/".base64_encode($employees->id)}}">
                                    <div class="col-md-3">
                                        <div class="team_profile client_project_details">
                                            <div class="profile_container">
                                            <h4><b>({{$employees->employeeId}}) {{$employees->emp_first_name}} {{$employees->emp_lastName}}</b></h4>
                                            </div>
                                            @if($employees->emp_photo)
                                            <img src="{{asset('employee_image/'.$employees->emp_photo)}}">
                                            @endif
                                        </div>
                                    </div>
                                  </a>
                                  @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                   @endif
                    @if(isset($data[0]->department_name))
                    <h4 class="text-center">Department: {{$data[0]->department_name}}</h4>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-blue">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <i class="fa fa-tasks" style="font-size: 20px;"></i>
                                            Employee
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                  @foreach($employee  as $employees)
                                  <a target="_blank" href="{{url('employee/profile')."/".base64_encode($employees->id)}}">
                                    <div class="col-md-3">
                                        <div class="team_profile client_project_details">

                                            <div class="profile_container">
                                            <h4><b>({{$employees->employeeId}}) {{$employees->emp_first_name}} {{$employees->emp_lastName}}</b></h4>
                                            </div>
                                            @if($employees->emp_photo)
                                            <img src="{{asset('employee_image/'.$employees->emp_photo)}}">
                                            @endif
                                        </div>
                                    </div>
                                  </a>
                                  @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @elseif(isset($data[0]->designation_name))
                    <h4 class="text-center">Designation: {{$data[0]->designation_name}}</h4>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-blue">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <i class="fa fa-tasks" style="font-size: 20px;"></i>
                                            Employee
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                  @foreach($employee  as $employees)
                                  <a target="_blank" href="{{url('employee/profile')."/".base64_encode($employees->id)}}">
                                    <div class="col-md-3">
                                        <div class="team_profile client_project_details">
                                            <div class="profile_container">
                                            <h4><b>({{$employees->employeeId}}) {{$employees->emp_first_name}} {{$employees->emp_lastName}}</b></h4>
                                            </div>
                                            @if($employees->emp_photo)
                                            <img src="{{asset('employee_image/'.$employees->emp_photo)}}">
                                            @endif
                                        </div>
                                    </div>
                                  </a>
                                  @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif

                    @if(isset($data[0]->employeeId))
                    <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-blue">
                                    <div class="panel-body">
                                       <a target="_blank" href="{{url('employee/profile')."/".base64_encode($data[0]->emp_id)}}">
                                           @foreach($employee  as $employees)
                                        <div class="col-md-3">
                                            <div class="team_profile client_project_details">
                                                <div class="profile_container">
                                                <h4><b>({{$employees->employeeId}}) {{$employees->emp_first_name}} {{$employees->emp_lastName}}</b></h4>
                                                </div>
                                                @if($employees->emp_photo)
                                                <img src="{{asset('employee_image/'.$employees->emp_photo)}}">
                                                @endif
                                            </div>
                                        </div>
                                        @endforeach
                                      </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-blue">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-12">
                            <i class="fa fa-tasks" style="font-size: 20px;"></i>
                            File
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                  @foreach($data  as  $item)
                  <div id="clients-edit-wrapper">
                        <div class="close-wrapper">


                    <div class="col-md-3">
                        <div class="team_profile client_project_details">
                            <div class="profile_container">
                               <h6><b>{{$item->attachment}}</b></h6>
                            </div>

                            @if($item->attachment=='')
                                <h1 style="color:red">No Attachment</h1>
                            @else
                            <a target="_blank" href="{{asset('share_file/'.$item->attachment)}}"  class="btn btn-success btn-sm client_project_btn"><i class="fa fa-download"> </i> </a>
                            <a  onclick="return confirm('are you sure want to delete??')" href="{{url('admin/wise/file/share/delete/'.base64_encode($item->main_id))}}" class="btn btn-danger btn-sm client_project_btn"><i class="fa fa-trash-o"> </i> </a>
                            @endif
                        </div>
                    </div>

                        </div>
                  </div>

                  @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



