@extends('layout.master')
@section('title', 'File Shared List')
@section('content')
    <!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title">File Shared List</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="{{url('/')}}">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li><a href="#">File</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active">File Shared List</li>
        </ol>
        <div class="clearfix"></div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE-->


    <div class="page-content">
        <!--Flash Message Start-->
        {{ Html::script('corporate/js/sweetalert.min.js') }}
        @if(Session::has('success'))
        <script>
            var msg =' <?php echo Session::get('success');?>'
            swal(msg, "", "success");
        </script>
        @endif
        @if(Session::has('error'))
        <p id="alert_message" class="alert alert-error">{{ Session::get('error') }}</p>
        @endif
        <!--Flash Message End-->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-blue">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                File Shared List
                            </div>
                            <div class="col-md-6" style="text-align: right;">
                            <a href="{{url('file/share')}}" class="add-new-modal btn btn-success btn-round btn-sm"><i class="fa fa-plus"></i> Add New</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body table-responsive">
                        <table id="project_table" class="table table-striped table-bordered" >
                            <thead>
                            <tr>
                                <th>Employee</th>
                                <th>Branch</th>
                                <th>Department</th>
                                <th>Designation</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('extra_js')
    <script>
        $(document).ready(function(){
            //Clients List
            $('#project_table').DataTable({
                processing: true,
                serverSide: true,
                "order": [[ 2, "desc" ]],
                ajax:{
                    url: "{{ route('admin_file_share_list') }}",
                },
                columns:[

                    {
                        data: 'emp_first_name',
                        name: 'emp_first_name'
                    },

                    {
                        data: 'branch_name',
                        name: 'branch_name'

                    },

                    {
                        data: 'department_name',
                        name: 'department_name'
                    },

                    {
                        data: 'designation_name',
                        name: 'designation_name'
                    },
                    {
                        data: 'action_btn',
                        name: 'action_btn',
                        orderable: false
                    }
                ]
            });

        });
    </script>
@endsection
