@extends('layout.master')
@section('title', 'System Settings')
@section('content')

    <script>
        setTimeout(function() {
            $('#alert_message').fadeOut('fast');
        }, 5000);
    </script>
    <div class="page-content">

        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-default">
                    <div class="panel-heading"><h4><b>System Language </b></h4></div>
                    <div class="panel-body">
                        {!! Form::open(array('route' => 'change_language_selector', 'method' => 'POST')) !!}
                        <table class="" width="100%">
                            <tr>
                                <td style="padding:30px;" >
                                    <label class="rcontainer">
                                        <input id="EnglishLang" type="radio" <?php if(Auth::user()->system_language=='en'){ echo "checked"; } ?> value="en" name="languageSelector" required="">
                                        <span class="checkmark"></span>English
                                    </label>
                                    <br>
                                    <br>
                                    <label class="rcontainer">
                                        <input  id="BanglaLang"  <?php if(Auth::user()->system_language=='bn'){ echo "checked"; } ?> type="radio" value="bn" name="languageSelector" required="">
                                        <span class="checkmark"></span> বাংলা
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top:40px;" >
                                    <hr>
                                    <button onclick="return confirm('Are you sure to change?')" class="btn btn-success" type="submit"> <i class="fa fa-refresh"></i>&nbsp; &nbsp; Change System Language</button>
                                </td>
                            </tr>
                        </table>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function changeselector(val){
            if(val=='en'){
                document.getElementById('EnglishLang').checked = true;
            }else if(val=='bn'){
                document.getElementById('BanglaLang').checked = true;
            }else{
                alert("Please choose a Language.");
            }
        }
    </script>
@endsection
